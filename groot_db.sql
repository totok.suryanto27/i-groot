-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2017 at 01:21 
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `groot_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`id` int(11) NOT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `postal_code`, `name`, `province_id`, `updated_at`, `created_at`) VALUES
(1, NULL, 'Kab. Kepulauan Seribu', 1, NULL, NULL),
(2, NULL, 'Kota Jakarta Pusat', 1, NULL, NULL),
(3, NULL, 'Kota Jakarta Utara', 1, NULL, NULL),
(4, NULL, 'Kota Jakarta Barat', 1, NULL, NULL),
(5, NULL, 'Kota Jakarta Selatan', 1, NULL, NULL),
(6, NULL, 'Kota Jakarta Timur', 1, NULL, NULL),
(7, NULL, 'Kab. Bogor', 2, NULL, NULL),
(8, NULL, 'Kab. Sukabumi', 2, NULL, NULL),
(9, NULL, 'Kab. Cianjur', 2, NULL, NULL),
(10, NULL, 'Kab. Bandung', 2, NULL, NULL),
(11, NULL, 'Kab. Sumedang', 2, NULL, NULL),
(12, NULL, 'Kab. Garut', 2, NULL, NULL),
(13, NULL, 'Kab. Tasikmalaya', 2, NULL, NULL),
(14, NULL, 'Kab. Ciamis', 2, NULL, NULL),
(15, NULL, 'Kab. Kuningan', 2, NULL, NULL),
(16, NULL, 'Kab. Majalengka', 2, NULL, NULL),
(17, NULL, 'Kab. Cirebon', 2, NULL, NULL),
(18, NULL, 'Kab. Indramayu', 2, NULL, NULL),
(19, NULL, 'Kab. Subang', 2, NULL, NULL),
(20, NULL, 'Kab. Purwakarta', 2, NULL, NULL),
(21, NULL, 'Kab. Karawang', 2, NULL, NULL),
(22, NULL, 'Kab. Bekasi', 2, NULL, NULL),
(23, NULL, 'Kab. Bandung Barat', 2, NULL, NULL),
(24, NULL, 'Kota Bandung', 2, NULL, NULL),
(25, NULL, 'Kota Bogor', 2, NULL, NULL),
(26, NULL, 'Kota Sukabumi', 2, NULL, NULL),
(27, NULL, 'Kota Cirebon', 2, NULL, NULL),
(28, NULL, 'Kota Bekasi', 2, NULL, NULL),
(29, NULL, 'Kota Depok', 2, NULL, NULL),
(30, NULL, 'Kota Cimahi', 2, NULL, NULL),
(31, NULL, 'Kota Tasikmalaya', 2, NULL, NULL),
(32, NULL, 'Kota Banjar', 2, NULL, NULL),
(33, NULL, 'Kab. Cilacap', 3, NULL, NULL),
(34, NULL, 'Kab. Banyumas', 3, NULL, NULL),
(35, NULL, 'Kab. Purbalingga', 3, NULL, NULL),
(36, NULL, 'Kab. Banjarnegara', 3, NULL, NULL),
(37, NULL, 'Kab. Kebumen', 3, NULL, NULL),
(38, NULL, 'Kab. Purworejo', 3, NULL, NULL),
(39, NULL, 'Kab. Wonosobo', 3, NULL, NULL),
(40, NULL, 'Kab. Magelang', 3, NULL, NULL),
(41, NULL, 'Kab. Boyolali', 3, NULL, NULL),
(42, NULL, 'Kab. Klaten', 3, NULL, NULL),
(43, NULL, 'Kab. Sukoharjo', 3, NULL, NULL),
(44, NULL, 'Kab. Wonogiri', 3, NULL, NULL),
(45, NULL, 'Kab. Karanganyar', 3, NULL, NULL),
(46, NULL, 'Kab. Sragen', 3, NULL, NULL),
(47, NULL, 'Kab. Grobogan', 3, NULL, NULL),
(48, NULL, 'Kab. Blora', 3, NULL, NULL),
(49, NULL, 'Kab. Rembang', 3, NULL, NULL),
(50, NULL, 'Kab. Pati', 3, NULL, NULL),
(51, NULL, 'Kab. Kudus', 3, NULL, NULL),
(52, NULL, 'Kab. Jepara', 3, NULL, NULL),
(53, NULL, 'Kab. Demak', 3, NULL, NULL),
(54, NULL, 'Kab. Semarang', 3, NULL, NULL),
(55, NULL, 'Kab. Temanggung', 3, NULL, NULL),
(56, NULL, 'Kab. Kendal', 3, NULL, NULL),
(57, NULL, 'Kab. Batang', 3, NULL, NULL),
(58, NULL, 'Kab. Pekalongan', 3, NULL, NULL),
(59, NULL, 'Kab. Pemalang', 3, NULL, NULL),
(60, NULL, 'Kab. Tegal', 3, NULL, NULL),
(61, NULL, 'Kab. Brebes', 3, NULL, NULL),
(62, NULL, 'Kota Magelang', 3, NULL, NULL),
(63, NULL, 'Kota Surakarta', 3, NULL, NULL),
(64, NULL, 'Kota Salatiga', 3, NULL, NULL),
(65, NULL, 'Kota Semarang', 3, NULL, NULL),
(66, NULL, 'Kota Pekalongan', 3, NULL, NULL),
(67, NULL, 'Kota Tegal', 3, NULL, NULL),
(68, NULL, 'Kab. Bantul', 4, NULL, NULL),
(69, NULL, 'Kab. Sleman', 4, NULL, NULL),
(70, NULL, 'Kab. Gunung Kidul', 4, NULL, NULL),
(71, NULL, 'Kab. Kulon Progo', 4, NULL, NULL),
(72, NULL, 'Kota Yogyakarta', 4, NULL, NULL),
(73, NULL, 'Kab. Gresik', 5, NULL, NULL),
(74, NULL, 'Kab. Sidoarjo', 5, NULL, NULL),
(75, NULL, 'Kab. Mojokerto', 5, NULL, NULL),
(76, NULL, 'Kab. Jombang', 5, NULL, NULL),
(77, NULL, 'Kab. Bojonegoro', 5, NULL, NULL),
(78, NULL, 'Kab. Tuban', 5, NULL, NULL),
(79, NULL, 'Kab. Lamongan', 5, NULL, NULL),
(80, NULL, 'Kab. Madiun', 5, NULL, NULL),
(81, NULL, 'Kab. Ngawi', 5, NULL, NULL),
(82, NULL, 'Kab. Magetan', 5, NULL, NULL),
(83, NULL, 'Kab. Ponorogo', 5, NULL, NULL),
(84, NULL, 'Kab. Pacitan', 5, NULL, NULL),
(85, NULL, 'Kab. Kediri', 5, NULL, NULL),
(86, NULL, 'Kab. Nganjuk', 5, NULL, NULL),
(87, NULL, 'Kab. Blitar', 5, NULL, NULL),
(88, NULL, 'Kab. Tulungagung', 5, NULL, NULL),
(89, NULL, 'Kab. Trenggalek', 5, NULL, NULL),
(90, NULL, 'Kab. Malang', 5, NULL, NULL),
(91, NULL, 'Kab. Pasuruan', 5, NULL, NULL),
(92, NULL, 'Kab. Probolinggo', 5, NULL, NULL),
(93, NULL, 'Kab. Lumajang', 5, NULL, NULL),
(94, NULL, 'Kab. Bondowoso', 5, NULL, NULL),
(95, NULL, 'Kab. Situbondo', 5, NULL, NULL),
(96, NULL, 'Kab. Jember', 5, NULL, NULL),
(97, NULL, 'Kab. Banyuwangi', 5, NULL, NULL),
(98, NULL, 'Kab. Pamekasan', 5, NULL, NULL),
(99, NULL, 'Kab. Sampang', 5, NULL, NULL),
(100, NULL, 'Kab. Sumenep', 5, NULL, NULL),
(101, NULL, 'Kab. Bangkalan', 5, NULL, NULL),
(102, NULL, 'Kota Surabaya', 5, NULL, NULL),
(103, NULL, 'Kota Malang', 5, NULL, NULL),
(104, NULL, 'Kota Madiun', 5, NULL, NULL),
(105, NULL, 'Kota Kediri', 5, NULL, NULL),
(106, NULL, 'Kota Mojokerto', 5, NULL, NULL),
(107, NULL, 'Kota Blitar', 5, NULL, NULL),
(108, NULL, 'Kota Pasuruan', 5, NULL, NULL),
(109, NULL, 'Kota Probolinggo', 5, NULL, NULL),
(110, NULL, 'Kota Batu', 5, NULL, NULL),
(111, '75869', 'Sorong', 32, '2015-09-06', '2015-09-06'),
(112, '58697', 'Malaka', 31, '2015-09-06', '2015-09-06');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `region_id`, `updated_at`, `created_at`) VALUES
(1, 'Indonesia', 1, NULL, NULL),
(2, 'Japan', 1, '2015-09-06', '2015-09-06'),
(3, 'South Korea', 1, '2015-09-06', '2015-09-06');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
`id` int(11) NOT NULL,
  `position` varchar(100) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `gender` varchar(25) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `city_id` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `position`, `full_name`, `gender`, `phone`, `email`, `address`, `city_id`, `updated_at`, `created_at`) VALUES
(1, 'Programmer', 'Sofyan', 'Male', '6281303030', 'sofyan@gmail.com', 'Kota Tegal', 2, '2016-12-17', '2016-12-17'),
(2, 'Management', 'Imad', 'Male', '6281738333444', 'imad@gmail.com', 'Kota Mojokerto', 85, '2016-12-17', '2016-12-17'),
(3, 'Design Grafis', 'Nugraha', 'Male', '62818142946698', 'nunug@gmail.com', 'Kab. Madiun', 76, '2016-12-17', '2016-12-17'),
(4, 'Programmer', 'Totok', 'Male', '62816784644372', 'totok@gmail.com', 'Kab. Sukabumi', 2, '2016-12-17', '2016-12-17'),
(5, 'Management', 'Siholoam Putri', 'Female', '081 303030', 'putri@siholoam.com', 'jalan Medan Barat', 2, '2017-01-19', '2017-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
`id` int(11) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `deskripsi` text,
  `tgl_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `harga_beli` double(10,2) NOT NULL,
  `harga_jual` double(10,2) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `jml_stok` int(50) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `goods`
--

INSERT INTO `goods` (`id`, `kode_barang`, `nama_barang`, `deskripsi`, `tgl_input`, `harga_beli`, `harga_jual`, `kategori_id`, `jml_stok`, `satuan`, `updated_at`, `created_at`) VALUES
(1, 'E00BLH18', 'BOLA LAMPU HANNOCHS 18 WATT', 'Bola lampu', '2017-01-19 00:19:13', 12000.00, 13000.00, 12, 10, 'pcs', '2017-01-19 00:19:13', '2017-01-19 00:19:13'),
(2, 'E00KLW40', 'KAP LAMPU WD E40 + FITTING KERAMIK', 'KAP LAMPU WD E40 + FITTING KERAMIK', '2017-01-19 00:20:23', 20000.00, 21000.00, 12, 11, 'PCS', '2017-01-19 00:20:23', '2017-01-19 00:20:23'),
(3, 'E00KYH44', 'KABEL NYH 4 X 4 MM', 'Kabel-kabel', '2017-01-19 00:42:11', 55000.00, 150000.00, 12, 10, 'ROLL', '2017-01-19 00:42:11', '2017-01-19 00:42:11'),
(4, 'E00MCC40', 'MCCB C-40A', 'MCB', '2017-01-19 00:43:01', 55000.00, 150000.00, 12, 10, 'PCS', '2017-01-19 00:43:01', '2017-01-19 00:43:01'),
(7, 'E00MCL25', 'M. CONTECTOR SCHENNEIDER TYPE:LC1E 25-10M5', 'Konektor', '2016-07-24 17:00:00', 55000.00, 150000.00, 12, 10, 'PC', '2017-01-19 00:44:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `goodscats`
--

CREATE TABLE IF NOT EXISTS `goodscats` (
  `kode_kategori` char(5) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
`id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `goodscats`
--

INSERT INTO `goodscats` (`kode_kategori`, `nama_kategori`, `id`, `updated_at`, `created_at`) VALUES
('C001', 'Barang Mentah', 6, '2017-01-18 04:39:53', NULL),
('C006', 'Peralatan Kelapa Sawit', 12, '2017-01-18 04:38:05', '2017-01-18 04:38:05');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `min_salary` int(11) NOT NULL,
  `max_salary` int(11) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `min_salary`, `max_salary`, `updated_at`, `created_at`) VALUES
(1, 'Programmer', 21000, 32000, NULL, NULL),
(2, 'System Analyst', 22000, 31000, NULL, NULL),
(3, 'Network Engineer', 17000, 36000, NULL, NULL),
(4, 'Accounting', 19000, 36000, NULL, NULL),
(5, 'Front Office', 20000, 30000, NULL, NULL),
(6, 'Archivement', 21000, 29000, NULL, NULL),
(7, 'Security', 16000, 28000, NULL, NULL),
(8, 'Cleaning Services', 13000, 19000, NULL, NULL),
(9, 'Management', 30000, 40000, NULL, NULL),
(10, 'Supreyor', 19000, 25000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pelanggan`
--

CREATE TABLE IF NOT EXISTS `kategori_pelanggan` (
`id` int(11) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_pelanggan`
--

INSERT INTO `kategori_pelanggan` (`id`, `nama_kategori`, `keterangan`, `updated_at`, `created_at`) VALUES
(1, 'Pembeli Potensial', '', '2017-01-19 01:15:50', NULL),
(2, 'Pembeli Langganan', 'pembeli yang sudah berlangganan', '2017-01-19 01:13:56', '2017-01-19 01:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE IF NOT EXISTS `pelanggan` (
  `kode_pelanggan` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `nomor_telp` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
`id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`kode_pelanggan`, `nama_pelanggan`, `nomor_telp`, `alamat`, `id`, `id_kategori`, `updated_at`, `created_at`) VALUES
('P0003', 'PT Sekar Abadi Putra', '081 303030', 'Jalan Medan Barat', 3, 1, '2017-01-19 01:31:11', '2017-01-19 01:31:11'),
('P0004', 'PT Sekar Abadi Jaya', '081 3030301', 'Bandung', 4, 1, '2017-01-19 01:32:00', '2017-01-19 01:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
`id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `country_id` int(11) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `country_id`, `updated_at`, `created_at`) VALUES
(1, 'DKI Jakarta', 1, NULL, NULL),
(2, 'Jawa Barat', 1, NULL, NULL),
(3, 'Jawa Tengah', 1, NULL, NULL),
(4, 'DI Yogyakarta', 1, NULL, NULL),
(5, 'Jawa Timur', 1, NULL, NULL),
(6, 'Nanggroe Aceh Darussalam', 1, NULL, NULL),
(7, 'Sumatera Utara', 1, NULL, NULL),
(8, 'Sumatera Barat', 1, NULL, NULL),
(9, 'Riau', 1, NULL, NULL),
(10, 'Jambi', 1, NULL, NULL),
(11, 'Sumatera Selatan', 1, NULL, NULL),
(12, 'Lampung', 1, NULL, NULL),
(13, 'Kalimantan Barat', 1, NULL, NULL),
(14, 'Kalimanatan Tengah', 1, NULL, NULL),
(15, 'Kalimantan Selatan', 1, NULL, NULL),
(16, 'Kalimantan Timur', 1, NULL, NULL),
(17, 'Sulawesi Utara', 1, NULL, NULL),
(18, 'Sulawesi Tengah', 1, NULL, NULL),
(19, 'Sulawesi Selatan', 1, NULL, NULL),
(20, 'Sulawesi Tenggara', 1, NULL, NULL),
(21, 'Maluku', 1, NULL, NULL),
(22, 'Bali', 1, NULL, NULL),
(23, 'Nusa Tenggara Barat', 1, NULL, NULL),
(24, 'Nusa Tenggara Timur', 1, NULL, NULL),
(25, 'Papua', 1, NULL, NULL),
(26, 'Bengkulu', 1, NULL, NULL),
(27, 'Maluku Utara', 1, NULL, NULL),
(28, 'Banten', 1, NULL, NULL),
(29, 'Kep. Bangka Belitung', 1, NULL, NULL),
(30, 'Gorontalo', 1, NULL, NULL),
(31, 'Kep. Riau', 1, NULL, NULL),
(32, 'Papua Barat ', 1, NULL, NULL),
(33, 'Sulawesi Barat', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
`id` int(11) NOT NULL,
  `nomer_order_req` varchar(50) NOT NULL,
  `tgl_in_trx` date DEFAULT NULL,
  `tgl_out_trx` date DEFAULT NULL,
  `departement` varchar(50) NOT NULL,
  `keperluan` varchar(70) NOT NULL,
  `urgentity` varchar(30) NOT NULL,
  `justification` varchar(80) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type_trx` int(11) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  `purchase_date` varchar(45) DEFAULT NULL,
  `nomer_order_purc` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`id`, `nomer_order_req`, `tgl_in_trx`, `tgl_out_trx`, `departement`, `keperluan`, `urgentity`, `justification`, `state`, `type_trx`, `updated_at`, `created_at`, `purchase_date`, `nomer_order_purc`) VALUES
(1, 'RO-201701250013', '2017-01-31', '2017-01-31', 'PKS 13', 'Stock Gudang dan Pisau Bucket Well Lowder Kesebelas', 'Biasa', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Kesebelas', 2, 2, '2017-01-31', '2017-01-25', '2017-01-25', 'PO-201701250001'),
(2, 'R-201701250007', '2017-01-31', '2017-01-31', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 'Top Urgen', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 2, 2, '2017-01-31', '2017-01-25', '2017-01-25', 'PO-201701250002'),
(4, 'R-201701250008', '2017-01-31', '2017-01-31', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Keenam', 'Top Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Keenam', 2, 2, '2017-01-31', '2017-01-31', '2017-01-31', 'PO-201701310003'),
(7, 'RO-201701310014', '2017-01-31', '2017-01-31', 'PKS 20', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 20', 'Top Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ke 20', 3, 2, '2017-01-31', '2017-01-31', '2017-01-31', 'PO-201701310005'),
(8, 'RO-201701310015', '2017-01-31', '2017-01-31', 'PKS 21', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 21', 'EDD', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 21', 3, 2, '2017-01-31', '2017-01-31', '2017-01-31', 'PO-201701310008'),
(9, 'RO-201701310017', '2017-01-31', '2017-01-31', 'PKS 23', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 23', 'Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ke 23', 2, 2, '2017-01-31', '2017-01-31', '2017-01-31', 'PO-201701310009');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_product`
--

CREATE TABLE IF NOT EXISTS `purchase_product` (
`inv_prd` int(11) NOT NULL,
  `id_inv` int(11) NOT NULL,
  `id_brg` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sub_total` int(30) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_product`
--

INSERT INTO `purchase_product` (`inv_prd`, `id_inv`, `id_brg`, `quantity`, `sub_total`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 1, 12100, '2017-01-31', '2017-01-31'),
(2, 1, 4, 1, 20100, '2017-01-31', '2017-01-31'),
(3, 4, 2, 1, 12200, '2017-01-31', '2017-01-31'),
(4, 4, 3, 1, 20000, '2017-01-31', '2017-01-31'),
(5, 4, 4, 1, 20100, '2017-01-31', '2017-01-31'),
(6, 2, 9, 1, 57000, '2017-01-31', '2017-01-31'),
(7, 2, 10, 2, 112000, '2017-01-31', '2017-01-31'),
(8, 9, 7, 1, 54800, '2017-01-31', '2017-01-31'),
(9, 9, 2, 1, 56000, '2017-01-31', '2017-01-31'),
(10, 9, 3, 1, 57000, '2017-01-31', '2017-01-31');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_responsible`
--

CREATE TABLE IF NOT EXISTS `purchase_responsible` (
`inv_resp` int(11) NOT NULL,
  `id_inv` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_responsible`
--

INSERT INTO `purchase_responsible` (`inv_resp`, `id_inv`, `id_employee`, `keterangan`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'madeby', '2017-01-31', '2017-01-31 08:28:27'),
(2, 1, 2, 'requestby', '2017-01-31', '2017-01-31 08:28:27'),
(3, 1, 3, 'verifiedby', '2017-01-31', '2017-01-31 08:28:27'),
(4, 1, 4, 'approvedby', '2017-01-31', '2017-01-31 08:28:28'),
(5, 4, 2, 'madeby', '2017-01-31', '2017-01-31 09:35:32'),
(6, 4, 5, 'requestby', '2017-01-31', '2017-01-31 09:35:32'),
(7, 4, 1, 'verifiedby', '2017-01-31', '2017-01-31 09:35:32'),
(8, 4, 4, 'approvedby', '2017-01-31', '2017-01-31 09:35:32'),
(9, 2, 3, 'madeby', '2017-01-31', '2017-01-31 09:42:54'),
(10, 2, 3, 'requestby', '2017-01-31', '2017-01-31 09:42:54'),
(11, 2, 5, 'verifiedby', '2017-01-31', '2017-01-31 09:42:54'),
(12, 2, 1, 'approvedby', '2017-01-31', '2017-01-31 09:42:54'),
(13, 9, 2, 'madeby', '2017-01-31', '2017-01-31 10:29:05'),
(14, 9, 1, 'requestby', '2017-01-31', '2017-01-31 10:29:05'),
(15, 9, 5, 'verifiedby', '2017-01-31', '2017-01-31 10:29:05'),
(16, 9, 3, 'approvedby', '2017-01-31', '2017-01-31 10:29:05');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'Asia', NULL, NULL),
(2, 'Europe', '2015-09-05', '2015-09-05'),
(3, 'Nort America', '2015-09-06', '2015-09-06'),
(4, 'South America', '2015-09-06', '2015-09-06'),
(5, 'Australia', '2015-09-06', '2015-09-06'),
(6, 'Pacific', '2015-09-06', '2015-09-06'),
(7, 'Africa', '2015-09-06', '2015-09-06'),
(8, 'Antartica', '2015-09-06', '2015-09-06');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
`id` int(11) NOT NULL,
  `nomer_order` varchar(50) NOT NULL,
  `tgl_in_trx` date DEFAULT NULL,
  `tgl_out_trx` date DEFAULT NULL,
  `departement` varchar(50) NOT NULL,
  `keperluan` varchar(70) NOT NULL,
  `urgentity` varchar(30) NOT NULL,
  `justification` varchar(80) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type_trx` int(11) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id`, `nomer_order`, `tgl_in_trx`, `tgl_out_trx`, `departement`, `keperluan`, `urgentity`, `justification`, `state`, `type_trx`, `updated_at`, `created_at`) VALUES
(1, 'R-001', '2016-12-07', NULL, 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder', 'Top Urgen', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 1, 1, '2017-01-19', '2017-01-19'),
(2, 'R-002', '2016-12-15', NULL, 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder yang Kedua', 'Urgen', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 1, 2, '2017-01-19', '2017-01-19'),
(7, 'R-201701250007', '2017-01-25', '2017-01-25', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 'Top Urgen', 'Stock Gudang dan Pisau Bucket Well Lowder Kelima', 3, 2, '2017-01-25', '2017-01-25'),
(8, 'R-201701250008', '2017-01-31', '2017-01-31', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Keenam', 'Top Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Keenam', 3, 2, '2017-01-31', '2017-01-25'),
(9, 'R-201701250009', '2017-01-25', '2017-01-25', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Ketujuh', 'Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ketujuh', 3, 2, '2017-01-25', '2017-01-25'),
(10, 'R-201701250010', '2017-01-25', '2017-01-25', 'PKS', 'Stock Gudang dan Pisau Bucket Well Lowder Kedelapan', 'Top Urgen', 'Stock Gudang dan Pisau Bucket Well Lowder Kedelapan', 3, 2, '2017-01-25', '2017-01-25'),
(11, 'R-201701250011', '2017-01-25', '2017-01-25', 'PKS 2', 'Stock Gudang dan Pisau Bucket Well Lowder Kesembilan', 'Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Kesembilan', 2, 1, '2017-01-25', '2017-01-25'),
(12, 'R-201701250012', '2017-01-25', '2017-01-25', 'PKS 12', 'Stock Gudang dan Pisau Bucket Well Lowder Kesepuluh', 'EDD', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Kesepuluh', 3, 2, '2017-01-25', '2017-01-25'),
(13, 'RO-201701250013', '2017-01-25', '2017-01-25', 'PKS 13', 'Stock Gudang dan Pisau Bucket Well Lowder Kesebelas', 'Biasa', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Kesebelas', 3, 2, '2017-01-25', '2017-01-25'),
(14, 'RO-201701310014', '2017-01-31', '2017-01-31', 'PKS 20', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 20', 'Top Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ke 20', 3, 2, '2017-01-31', '2017-01-31'),
(15, 'RO-201701310015', '2017-01-31', '2017-01-31', 'PKS 21', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 21', 'EDD', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 21', 3, 2, '2017-01-31', '2017-01-31'),
(16, 'RO-201701310016', '2017-01-31', '2017-01-31', 'PKS 22', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 22', 'Biasa', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ke 22', 1, 1, '2017-01-31', '2017-01-31'),
(17, 'RO-201701310017', '2017-01-31', '2017-01-31', 'PKS 23', 'Stock Gudang dan Pisau Bucket Well Lowder Ke 23', 'Urgen', 'Pembelian Stock Gudang dan Pisau Bucket Well Lowder Ke 23', 3, 2, '2017-01-31', '2017-01-31');

-- --------------------------------------------------------

--
-- Table structure for table `request_product`
--

CREATE TABLE IF NOT EXISTS `request_product` (
`inv_prd` int(11) NOT NULL,
  `id_inv` int(11) NOT NULL,
  `id_brg` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sub_total` int(30) NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_product`
--

INSERT INTO `request_product` (`inv_prd`, `id_inv`, `id_brg`, `quantity`, `sub_total`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 1, 110000, '2017-01-19', '2017-01-19'),
(2, 1, 2, 1, 110000, '2017-01-19', '2017-01-19'),
(14, 12, 7, 2, 110000, '2017-01-25', '2017-01-25'),
(16, 10, 4, 2, 110000, '2017-01-25', '2017-01-25'),
(19, 9, 2, 2, 40000, '2017-01-25', '2017-01-25'),
(24, 7, 7, 3, 165000, '2017-01-25', '2017-01-25'),
(32, 8, 1, 1, 12000, '2017-01-31', '2017-01-31'),
(33, 8, 2, 2, 40000, '2017-01-31', '2017-01-31'),
(39, 16, 1, 1, 12000, '2017-01-31', '2017-01-31'),
(40, 16, 2, 2, 40000, '2017-01-31', '2017-01-31'),
(41, 17, 4, 1, 55000, '2017-01-31', '2017-01-31'),
(42, 17, 7, 1, 55000, '2017-01-31', '2017-01-31'),
(45, 14, 3, 1, 55000, '2017-01-31', '2017-01-31');

-- --------------------------------------------------------

--
-- Table structure for table `request_responsible`
--

CREATE TABLE IF NOT EXISTS `request_responsible` (
`inv_resp` int(11) NOT NULL,
  `id_inv` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_responsible`
--

INSERT INTO `request_responsible` (`inv_resp`, `id_inv`, `id_employee`, `keterangan`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'madeby', NULL, NULL),
(2, 1, 2, 'requestby', NULL, NULL),
(19, 11, 3, 'madeby', '2017-01-25', '2017-01-25 09:44:34'),
(20, 11, 5, 'requestby', '2017-01-25', '2017-01-25 09:44:34'),
(21, 11, 1, 'verifiedby', '2017-01-25', '2017-01-25 09:44:34'),
(22, 11, 4, 'approvedby', '2017-01-25', '2017-01-25 09:44:34'),
(31, 12, 2, 'madeby', '2017-01-25', '2017-01-25 10:57:12'),
(32, 12, 2, 'requestby', '2017-01-25', '2017-01-25 10:57:12'),
(33, 12, 1, 'verifiedby', '2017-01-25', '2017-01-25 10:57:12'),
(34, 12, 4, 'approvedby', '2017-01-25', '2017-01-25 10:57:12'),
(39, 10, 2, 'madeby', '2017-01-25', '2017-01-25 11:28:03'),
(40, 10, 2, 'requestby', '2017-01-25', '2017-01-25 11:28:03'),
(41, 10, 1, 'verifiedby', '2017-01-25', '2017-01-25 11:28:03'),
(42, 10, 4, 'approvedby', '2017-01-25', '2017-01-25 11:28:03'),
(51, 9, 2, 'madeby', '2017-01-25', '2017-01-25 11:31:37'),
(52, 9, 2, 'requestby', '2017-01-25', '2017-01-25 11:31:37'),
(53, 9, 5, 'verifiedby', '2017-01-25', '2017-01-25 11:31:37'),
(54, 9, 1, 'approvedby', '2017-01-25', '2017-01-25 11:31:37'),
(59, 13, 2, 'madeby', '2017-01-25', '2017-01-25 11:35:19'),
(60, 13, 2, 'requestby', '2017-01-25', '2017-01-25 11:35:19'),
(61, 13, 3, 'verifiedby', '2017-01-25', '2017-01-25 11:35:19'),
(62, 13, 5, 'approvedby', '2017-01-25', '2017-01-25 11:35:19'),
(71, 7, 3, 'madeby', '2017-01-25', '2017-01-25 11:42:25'),
(72, 7, 3, 'requestby', '2017-01-25', '2017-01-25 11:42:25'),
(73, 7, 5, 'verifiedby', '2017-01-25', '2017-01-25 11:42:25'),
(74, 7, 1, 'approvedby', '2017-01-25', '2017-01-25 11:42:25'),
(75, 1, 3, 'verifiedby', NULL, NULL),
(76, 1, 4, 'approvedby', NULL, NULL),
(109, 8, 2, 'madeby', '2017-01-31', '2017-01-31 09:24:55'),
(110, 8, 5, 'requestby', '2017-01-31', '2017-01-31 09:24:55'),
(111, 8, 1, 'verifiedby', '2017-01-31', '2017-01-31 09:24:55'),
(112, 8, 4, 'approvedby', '2017-01-31', '2017-01-31 09:24:56'),
(145, 14, 5, 'madeby', '2017-01-31', '2017-01-31 10:11:23'),
(146, 14, 2, 'requestby', '2017-01-31', '2017-01-31 10:11:23'),
(147, 14, 1, 'verifiedby', '2017-01-31', '2017-01-31 10:11:23'),
(148, 14, 3, 'approvedby', '2017-01-31', '2017-01-31 10:11:23'),
(153, 15, 3, 'madeby', '2017-01-31', '2017-01-31 10:22:41'),
(154, 15, 2, 'requestby', '2017-01-31', '2017-01-31 10:22:41'),
(155, 15, 1, 'verifiedby', '2017-01-31', '2017-01-31 10:22:41'),
(156, 15, 5, 'approvedby', '2017-01-31', '2017-01-31 10:22:41'),
(157, 16, 3, 'madeby', '2017-01-31', '2017-01-31 10:23:02'),
(158, 16, 5, 'requestby', '2017-01-31', '2017-01-31 10:23:02'),
(159, 16, 2, 'verifiedby', '2017-01-31', '2017-01-31 10:23:02'),
(160, 16, 1, 'approvedby', '2017-01-31', '2017-01-31 10:23:02'),
(165, 17, 2, 'madeby', '2017-01-31', '2017-01-31 10:27:27'),
(166, 17, 1, 'requestby', '2017-01-31', '2017-01-31 10:27:27'),
(167, 17, 5, 'verifiedby', '2017-01-31', '2017-01-31 10:27:27'),
(168, 17, 3, 'approvedby', '2017-01-31', '2017-01-31 10:27:27');

-- --------------------------------------------------------

--
-- Table structure for table `state_trx`
--

CREATE TABLE IF NOT EXISTS `state_trx` (
`id` int(11) NOT NULL,
  `state_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state_trx`
--

INSERT INTO `state_trx` (`id`, `state_name`) VALUES
(1, 'Draft'),
(2, 'Posting for Approval'),
(3, 'Posting for PO'),
(4, 'Rejected'),
(5, 'Deleted'),
(6, 'Posting for Receive Order');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
`id` int(30) NOT NULL,
  `kode_supplier` varchar(10) NOT NULL,
  `nama_toko` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode_supplier`, `nama_toko`, `alamat`, `telepon`, `email`, `updated_at`, `created_at`) VALUES
(3, 'S0001', 'ELEVEN', 'JAMBI', '888', '-', NULL, NULL),
(5, 'S0003', 'Mekar Wangi', 'Jalan Coblong Dago sebelah Griya', '083210987', 'vcsd@mwangi.com', '2017-01-18 04:17:37', '2017-01-18 04:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_goods`
--

CREATE TABLE IF NOT EXISTS `supplier_goods` (
`id` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_goods` int(11) NOT NULL,
  `harga_beli` int(50) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_goods`
--

INSERT INTO `supplier_goods` (`id`, `id_supplier`, `id_goods`, `harga_beli`, `updated_at`, `created_at`) VALUES
(1, 3, 1, 12100, NULL, NULL),
(2, 5, 1, 12200, NULL, NULL),
(3, 3, 2, 20000, NULL, NULL),
(4, 5, 2, 20100, NULL, NULL),
(5, 3, 3, 55100, '2017-01-31', '2017-01-31'),
(6, 5, 3, 54900, '2017-01-31', '2017-01-31'),
(7, 3, 4, 54800, '2017-01-31', '2017-01-31'),
(8, 5, 4, 55100, '2017-01-31', '2017-01-31'),
(9, 3, 7, 57000, '2017-01-31', '2017-01-31'),
(10, 5, 7, 56000, '2017-01-31', '2017-01-31');

-- --------------------------------------------------------

--
-- Table structure for table `type_trx`
--

CREATE TABLE IF NOT EXISTS `type_trx` (
`id` int(11) NOT NULL,
  `code_type` char(5) NOT NULL,
  `nama_type` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_trx`
--

INSERT INTO `type_trx` (`id`, `code_type`, `nama_type`) VALUES
(1, 'RO', 'Request Order'),
(2, 'PO', 'Purchase Order'),
(3, 'RV', 'Received Order'),
(4, 'IV', 'Invoice');

-- --------------------------------------------------------

--
-- Table structure for table `urgentity`
--

CREATE TABLE IF NOT EXISTS `urgentity` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urgentity`
--

INSERT INTO `urgentity` (`id`, `name`) VALUES
(1, 'Top Urgen'),
(2, 'Urgen'),
(3, 'Biasa'),
(4, 'EDD');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_id`, `username`, `password`, `updated_at`, `created_at`) VALUES
(1, 1, 'sofyan', '202cb962ac59075b964b07152d234b70', '2015-09-07', '2015-09-07'),
(5, 3, 'nunug', '202cb962ac59075b964b07152d234b70', '2015-09-07', '2015-09-07'),
(6, 4, 'totok', '202cb962ac59075b964b07152d234b70', '2015-09-07', '2015-09-07'),
(7, 2, 'imad2', '202cb962ac59075b964b07152d234b70', '2017-01-18', '2017-01-18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`id`), ADD KEY `province_id` (`province_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`), ADD KEY `region_id` (`region_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
 ADD PRIMARY KEY (`id`), ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_tb_kategori_barang` (`kategori_id`);

--
-- Indexes for table `goodscats`
--
ALTER TABLE `goodscats`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_pelanggan`
--
ALTER TABLE `kategori_pelanggan`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_pelanggan_1_idx` (`id_kategori`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
 ADD PRIMARY KEY (`id`), ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_product`
--
ALTER TABLE `purchase_product`
 ADD PRIMARY KEY (`inv_prd`);

--
-- Indexes for table `purchase_responsible`
--
ALTER TABLE `purchase_responsible`
 ADD PRIMARY KEY (`inv_resp`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_product`
--
ALTER TABLE `request_product`
 ADD PRIMARY KEY (`inv_prd`);

--
-- Indexes for table `request_responsible`
--
ALTER TABLE `request_responsible`
 ADD PRIMARY KEY (`inv_resp`);

--
-- Indexes for table `state_trx`
--
ALTER TABLE `state_trx`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_goods`
--
ALTER TABLE `supplier_goods`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_trx`
--
ALTER TABLE `type_trx`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `urgentity`
--
ALTER TABLE `urgentity`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_id` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `goodscats`
--
ALTER TABLE `goodscats`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori_pelanggan`
--
ALTER TABLE `kategori_pelanggan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `purchase_product`
--
ALTER TABLE `purchase_product`
MODIFY `inv_prd` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `purchase_responsible`
--
ALTER TABLE `purchase_responsible`
MODIFY `inv_resp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `request_product`
--
ALTER TABLE `request_product`
MODIFY `inv_prd` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `request_responsible`
--
ALTER TABLE `request_responsible`
MODIFY `inv_resp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `state_trx`
--
ALTER TABLE `state_trx`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier_goods`
--
ALTER TABLE `supplier_goods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `type_trx`
--
ALTER TABLE `type_trx`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `urgentity`
--
ALTER TABLE `urgentity`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
ADD CONSTRAINT `countries_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`);

--
-- Constraints for table `goods`
--
ALTER TABLE `goods`
ADD CONSTRAINT `fk_barang_1` FOREIGN KEY (`kategori_id`) REFERENCES `goodscats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
ADD CONSTRAINT `fk_pelanggan_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_pelanggan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `provinces`
--
ALTER TABLE `provinces`
ADD CONSTRAINT `provinces_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
