<?php

class MasterController extends Controller{

	// BEGIN FUNGSI USER //
	public function getUser(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Users::where('username', 'like', '%'.$key.'%')->orderBy('id', 'desc')->paginate(10);
		}else{
			$data = Users::orderBy('id', 'desc')->paginate(10);
		}
		return View::make('home/dashboard',array())->nest('content', 'users/index',array('data'=>$data));
	}

	public function getAddUser(){
		return View::make('home/dashboard',array())->nest('content', 'users/add',array());
	}

	public function getSaveUser(){
		$id = Input::get('id');
		$employee_id = Input::get('employee_id');
		$username = Input::get('username');
		$pass1 = Input::get('password1');
		$pass2 = Input::get('password2');
		if($id){
			$exist_username = Users::where('username','=',$username)->where('id','!=',$id)->first();
			if(!empty($exist_username)){
				Session::flash('message', 'Sorry, Username was exists');
				return Redirect::to('users/edit/'.$id);
			}else{
				if($pass1!=$pass2){
					Session::flash('message', 'Sorry, Password not same');
					return Redirect::to('users/edit/'.$id);
				}else{
					if($employee_id){
						$user = Users::find($id);
						$user->username = $username;
						$user->employee_id = $employee_id;
						$user->password = md5($pass1);
						$user->save();
						Session::flash('message', 'The records are updated successfully');
						return Redirect::to('users');
					}else{
						$user = Users::find($id);
						$user->username = $username;
						$user->password = md5($pass1);
						$user->save();
						Session::flash('message', 'The records are updated successfully');
						return Redirect::to('users');
					}
				}	
			}	
		}else{
			$exist_username = Users::where('username','=',$username)->first();
			if(!empty($exist_username)){
				Session::flash('message', 'Sorry, Username was exists');
				return Redirect::to('users/add');
			}else{
				if($employee_id){
					$exists_employee = Users::where('employee_id','=',$employee_id)->first();
					if(!empty($exists_employee)){
						Session::flash('message', 'Sorry, Employee was exists');
						return Redirect::to('users/add');
					}else{
						if($pass1!=$pass2){
							Session::flash('message', 'Sorry, Password not same');
							return Redirect::to('users/add');
						}else{
							$user = new Users;
							$user->username = $username;
							$user->employee_id = $employee_id;
							$user->password = md5($pass1);
							$user->save();
							Session::flash('message', 'The records are inserted successfully');
							return Redirect::to('users');
						}	
					}
				}else{
					if($pass1!=$pass2){
						Session::flash('message', 'Sorry, Password not same');
						return Redirect::to('users/add');
					}else{
						$user = new Users;
						$user->username = $username;
						$user->password = md5($pass1);
						$user->save();
						Session::flash('message', 'The records are inserted successfully');
						return Redirect::to('users');
					}
				}
			}
			
		}
		
	}

	public function getShowUser($id){
		$data = Users::find($id);
		return View::make('home/dashboard',array())->nest('content', 'users/show',array('data'=>$data));
	}

	public function getEditUser($id){
		$data = Users::find($id);
		return View::make('home/dashboard',array())->nest('content', 'users/edit',array('data'=>$data));
	}

	public function getDeleteUser($id){
		$users = Users::find($id);
		$users->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('users');
	}

	public function getEmployeeUser(){
		$q = Input::get('q');
		$data = Employee::where('full_name', 'like', '%'.$q.'%')->orderBy('full_name', 'asc')->limit(10)->get();
		$array = array();
		foreach($data as $row){
			$array[] = array(
				'id'=>$row->id,
				'text'=>$row->full_name
			);
		}
		echo json_encode($array);
	}
	// END FUNGSI USER //

	// BEGIN FUNGSI SUPPLIER //
	public function getSupplier(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Supplier::where('nama_toko', 'like', '%'.$key.'%')->orderBy('updated_at', 'desc')->paginate(10);
		}else{
			$data = Supplier::orderBy('updated_at', 'desc')->paginate(10);
		}
		return View::make('home/dashboard',array())->nest('content', 'supplier/index',array('data'=>$data));
	}

	public function getSupplieradd(){
		return View::make('home/dashboard',array())->nest('content', 'supplier/add',array());
	}

	public function getSuppliersave(){ 
		$id = Input::get('id');
		if($id){
			$supplier = Supplier::find($id);
			$supplier->kode_supplier = Input::get('kode_supplier');
			$supplier->nama_toko = Input::get('nama_toko');
			$supplier->alamat = Input::get('alamat');
			$supplier->telepon = Input::get('telepon');
			$supplier->email = Input::get('email'); 
			$supplier->save();
			Session::flash('message', 'The records are updated successfully');
		}else{
			$supplier = new Supplier;
			$supplier->kode_supplier = Input::get('kode_supplier');
			$supplier->nama_toko = Input::get('nama_toko');
			$supplier->alamat = Input::get('alamat');
			$supplier->telepon = Input::get('telepon');
			$supplier->email = Input::get('email'); 
			$supplier->save();
			Session::flash('message', 'The records are inserted successfully');
		}
		return Redirect::to('supplier');  
	}
 
	public function getSupplieredit($id){
		$data = Supplier::find($id);
		return View::make('home/dashboard',array())->nest('content', 'supplier/edit',array('data'=>$data));
	}

	public function getSupplierdelete($id){
		$supplier = Supplier::find($id);
		$supplier->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('supplier');
	}
	// END FUNGSI SUPPLIER //

	// BEGIN FUNGSI Kategori Barang //
	public function getGoodscat(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Goodscats::where('nama_kategori', 'like', '%'.$key.'%')->orderBy('updated_at', 'desc')->paginate(10);
		}else{
			$data = Goodscats::orderBy('updated_at', 'desc')->paginate(10);
		}
		return View::make('home/dashboard',array())->nest('content', 'goods/index_cat',array('data'=>$data));
	}

	public function getGoodscatadd(){
		return View::make('home/dashboard',array())->nest('content', 'goods/add_cat',array());
	}

	public function getGoodscatsave(){ 
		$id = Input::get('id');
		if($id){
			$Goodscat = Goodscats::find($id);
			$Goodscat->kode_kategori = Input::get('kode_kategori');
			$Goodscat->nama_kategori = Input::get('nama_kategori'); 
			$Goodscat->save();
			Session::flash('message', 'The records are updated successfully');
		}else{
			$Goodscat = new Goodscats;
			$Goodscat->kode_kategori = Input::get('kode_kategori');
			$Goodscat->nama_kategori = Input::get('nama_kategori'); 
			$Goodscat->save();
			Session::flash('message', 'The records are inserted successfully');
		}
		return Redirect::to('goodscat');  
	}
 
	public function getGoodscatedit($id){
		$data = Goodscats::find($id);
		return View::make('home/dashboard',array())->nest('content', 'goods/edit_cat',array('data'=>$data));
	}

	public function getGoodscatdelete($id){
		$Goodscat = Goodscats::find($id);
		$Goodscat->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('goodscat');
	}
	// END FUNGSI Kategori Barang  //

	// BEGIN FUNGSI Barang //
	public function getGoods(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Goods::where('nama_barang', 'like', '%'.$key.'%')->orderBy('updated_at', 'desc')->paginate(10);
		}else{
			$data = Goods::orderBy('updated_at', 'desc')->paginate(10);
		}
		
		return View::make('home/dashboard',array())->nest('content', 'goods/index',array('data'=>$data));
	}

	public function getGoodsadd(){
		$type = Goodscats::all();
		return View::make('home/dashboard',array())->nest('content', 'goods/add',array('type'=>$type));
	}

	public function getGoodssave(){ 
		$id = Input::get('id');
		if($id){
			$Goods = Goods::find($id);
			$Goods->kode_barang = Input::get('kode_barang');
			$Goods->nama_barang = Input::get('nama_barang'); 
			$Goods->deskripsi = Input::get('deskripsi');
			$Goods->harga_beli = Input::get('harga_beli'); 
			$Goods->harga_jual = Input::get('harga_jual'); 
			$Goods->kategori_id = Input::get('kategori_id');
			$Goods->jml_stok = Input::get('jml_stok'); 
			$Goods->satuan = Input::get('satuan'); 
			$Goods->save();
			Session::flash('message', 'The records are updated successfully');
		}else{
			$Goods = new Goods;
			$Goods->kode_barang = Input::get('kode_barang');
			$Goods->nama_barang = Input::get('nama_barang'); 
			$Goods->deskripsi = Input::get('deskripsi');
			$Goods->harga_beli = Input::get('harga_beli'); 
			$Goods->harga_jual = Input::get('harga_jual'); 
			$Goods->kategori_id = Input::get('kategori_id');
			$Goods->jml_stok = Input::get('jml_stok'); 
			$Goods->satuan = Input::get('satuan'); 
			$Goods->save();
			Session::flash('message', 'The records are inserted successfully');
		}
		return Redirect::to('goods');  
	}
 
	public function getGoodsedit($id){
		$type = Goodscats::all();
		$data = Goods::find($id);
		return View::make('home/dashboard',array())->nest('content', 'goods/edit',array('data'=>$data,'type'=>$type));
	}

	public function getGoodsdelete($id){
		$Goods = Goods::find($id);
		$Goods->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('goods');
	}
	// END FUNGSI Barang  //


	// BEGIN FUNGSI Kategori Pelanggan //
	public function getCustomerscat(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Customerscat::where('nama_kategori', 'like', '%'.$key.'%')->orderBy('updated_at', 'desc')->paginate(10);
		}else{
			$data = Customerscat::orderBy('updated_at', 'desc')->paginate(10);
		}
		return View::make('home/dashboard',array())->nest('content', 'customers/index_cat',array('data'=>$data));
	}

	public function getCustomerscatadd(){
		return View::make('home/dashboard',array())->nest('content', 'customers/add_cat',array());
	}

	public function getCustomerscatsave(){ 
		$id = Input::get('id');
		if($id){
			$Customerscat = Customerscat::find($id);
			$Customerscat->nama_kategori = Input::get('nama_kategori'); 
			$Customerscat->keterangan = Input::get('keterangan');
			$Customerscat->save();
			Session::flash('message', 'The records are updated successfully');
		}else{
			$Customerscat = new Customerscat;
			$Customerscat->nama_kategori = Input::get('nama_kategori'); 
			$Customerscat->keterangan = Input::get('keterangan');
			$Customerscat->save();
			Session::flash('message', 'The records are inserted successfully');
		}
		return Redirect::to('customerscat');  
	}
 
	public function getCustomerscatedit($id){
		$data = Customerscat::find($id);
		return View::make('home/dashboard',array())->nest('content', 'customers/edit_cat',array('data'=>$data));
	}

	public function getCustomerscatdelete($id){
		$Goodscat = Customerscat::find($id);
		$Goodscat->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('customerscat');
	}
	// END FUNGSI Kategori Pelanggan  //

	// BEGIN FUNGSI Pelanggan //
	public function getCustomers(){
		$key = Input::get('search');
		if(isset($key)){
			$data = Customers::where('nama_pelanggan', 'like', '%'.$key.'%')->orderBy('updated_at', 'desc')->paginate(10);
		}else{
			$data = Customers::orderBy('updated_at', 'desc')->paginate(10);
		}
		
		return View::make('home/dashboard',array())->nest('content', 'customers/index',array('data'=>$data));
	}

	public function getCustomersadd(){
		$type = Customerscat::all();
		return View::make('home/dashboard',array())->nest('content', 'customers/add',array('type'=>$type));
	}

	public function getCustomerssave(){ 
		$id = Input::get('id');
		if($id){
			$Customers = Customers::find($id);
			$Customers->kode_pelanggan = Input::get('kode_pelanggan');
			$Customers->nama_pelanggan = Input::get('nama_pelanggan'); 
			$Customers->id_kategori = Input::get('id_kategori');
			$Customers->nomor_telp = Input::get('nomor_telp'); 
			$Customers->alamat = Input::get('alamat');  
			$Customers->save();
			Session::flash('message', 'The records are updated successfully');
		}else{
			$Customers = new Customers;
			$Customers->kode_pelanggan = Input::get('kode_pelanggan');
			$Customers->nama_pelanggan = Input::get('nama_pelanggan'); 
			$Customers->id_kategori = Input::get('id_kategori');
			$Customers->nomor_telp = Input::get('nomor_telp'); 
			$Customers->alamat = Input::get('alamat');  
			$Customers->save(); 
			Session::flash('message', 'The records are inserted successfully');
		}
		return Redirect::to('customers');  
	}
 
	public function getCustomersedit($id){
		$type = Customerscat::all();
		$data = Customers::find($id);
		return View::make('home/dashboard',array())->nest('content', 'customers/edit',array('data'=>$data,'type'=>$type));
	}

	public function getCustomersdelete($id){
		$Customers = Customers::find($id);
		$Customers->delete();
		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('customers');
	}
	// END FUNGSI Pelanggan  //


}