<?php

class InventoriesController extends Controller{

	// FUNGSI Request Order //
	public function getRoindex(){
		$key = Input::get('search');
		if(isset($key)){
			// $data = Inventories::where('type_trx','=', '1','nomer_order', 'like', '%'.$key.'%')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('request')
            ->join('state_trx', 'state_trx.id', '=', 'request.state') 
            ->join('type_trx', 'type_trx.id', '=', 'request.type_trx') 
            ->select('request.id', 'request.nomer_order','request.departement','request.keperluan','request.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('request.type_trx','=','1') 
            ->where('request.nomer_order','like','%'.$key.'%') 
            ->whereIn('request.state', [1, 4]) 
            ->orderBy('request.nomer_order', 'desc')->paginate(10);
		}else{
			// $data = Inventories::where('type_trx','=', '1')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('request')
            ->join('state_trx', 'state_trx.id', '=', 'request.state') 
            ->join('type_trx', 'type_trx.id', '=', 'request.type_trx') 
            ->select('request.id', 'request.nomer_order','request.departement','request.keperluan','request.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('request.type_trx','=','1')  
            ->whereIn('request.state', [1, 4]) 
            ->orderBy('request.nomer_order', 'desc')->paginate(10);
		} 
		return View::make('home/dashboard',array())->nest('content', 'inventories/roindex',array('data'=>$data));
	}

	public function getRoadd(){
		return View::make('home/dashboard',array())->nest('content', 'inventories/roadd',array());
	}

	public function getSavereq(){

		$id = Input::get('id');
		$year = date('Y');
		$month = date('m');
		$date = date('d');
		$now = date('Y-m-d');

		// Fungsi Update 
		if($id){  

			$inventory = Inventories::find($id);
			$inventory->nomer_order = Input::get('nomer_order');
			$inventory->departement = Input::get('departement');
			$inventory->keperluan = Input::get('keperluan');
			$inventory->urgentity = Input::get('urgentity');
			$inventory->justification = Input::get('justification');
			$inventory->state = Input::get('state');
			$inventory->type_trx = 1;
			$inventory->tgl_in_trx = $now;
			$inventory->tgl_out_trx = $now; 
			$inventory->save();

			//hapus detail penanggung jawab
			$bs = Requestresponsible::where('id_inv',$id);
			$bs->delete(); 
			$cek_bs = Requestresponsible::where('id_inv','=',$id)->first();
			if(empty($cek_bs)){
				if(Input::get('madeby')){
				$inventory_responsible1 = new Requestresponsible;  
				$inventory_responsible1->id_inv = $id;
				$inventory_responsible1->id_employee = Input::get('madeby');   
				$inventory_responsible1->keterangan = 'madeby';  
				$inventory_responsible1->save(); 	
				}
				if(Input::get('requestby')){
					$inventory_responsible2 = new Requestresponsible;  
					$inventory_responsible2->id_inv = $id;
					$inventory_responsible2->id_employee = Input::get('requestby');  
					$inventory_responsible2->keterangan = 'requestby';  
					$inventory_responsible2->save(); 
				}
				if(Input::get('verifiedby')){
					$inventory_responsible3 = new Requestresponsible;  
					$inventory_responsible3->id_inv = $id;
					$inventory_responsible3->id_employee = Input::get('verifiedby'); 
					$inventory_responsible3->keterangan = 'verifiedby';  
					$inventory_responsible3->save(); 
				}
				if(Input::get('approvedby')){
					$inventory_responsible4 = new Requestresponsible;  
					$inventory_responsible4->id_inv = $id;
					$inventory_responsible4->id_employee = Input::get('approvedby'); 
					$inventory_responsible4->keterangan = 'approvedby';  
					$inventory_responsible4->save(); 
				}
			} 

			//hapus detail produk
			// $br = Requestproduct::where('id_inv',$id);
			// $br->delete();

			// $cek_rp = Requestproduct::where('id_inv','=',$id)->first();
			// if(empty($cek_rp)){
				$good_id = Input::get('good_id');
				$jml_id = Input::get('jml_id');
				$price_id = Input::get('price_id');


				for($j=0;$j<count($good_id);$j++){ 
					// $inventory_product = new Requestproduct; 
					$inventory_product = Inventories::find($id); 
					$inventory_product->id_brg = $good_id[$j];
					$inventory_product->quantity = $jml_id[$j];
					$inventory_product->sub_total = $price_id[$j]; 
					$inventory_product->save();   
				}
			// }
 
		   Session::flash('message', 'The records are updated successfully');	
		   return Redirect::to('inventories/roindex');	  
		}else{
			// Fungsi Insert 
			$data = Inventories::orderBy('id', 'desc')->first();

			if(isset($data->id)){
				$index = $data->id+1;
			}else{
				$index = 1;
			}

			$x = strlen($index);
			$y = null;
			for($i=4;$i>$x;$i--){
				$y.= '0';
			}
			$y.=$index; 
			$request_code = 'RO-'.''.$year.''.$month.''.$date.''.$y;

			// Insert in Table Request 
			$inventory = new Inventories;
			$inventory->nomer_order = $request_code;
			$inventory->departement = Input::get('departement');
			$inventory->keperluan = Input::get('keperluan');
			$inventory->urgentity = Input::get('urgentity');
			$inventory->justification = Input::get('justification');
			$inventory->state = Input::get('state');
			$inventory->type_trx = 1;
			$inventory->tgl_in_trx = $now;
			$inventory->tgl_out_trx = $now; 
			$inventory->save();
			$last = Inventories::orderBy('id', 'desc')->first();
			$last_id = $last->id;

  			// Insert in Table Request Resposible
			if(Input::get('madeby')){
				$inventory_responsible1 = new Requestresponsible;  
				$inventory_responsible1->id_inv = $last_id;
				$inventory_responsible1->id_employee = Input::get('madeby');   
				$inventory_responsible1->keterangan = 'madeby';  
				$inventory_responsible1->save(); 	
			}
			if(Input::get('requestby')){
				$inventory_responsible2 = new Requestresponsible;  
				$inventory_responsible2->id_inv = $last_id;
				$inventory_responsible2->id_employee = Input::get('requestby');  
				$inventory_responsible2->keterangan = 'requestby';  
				$inventory_responsible2->save(); 
			}
			if(Input::get('verifiedby')){
				$inventory_responsible3 = new Requestresponsible;  
				$inventory_responsible3->id_inv = $last_id;
				$inventory_responsible3->id_employee = Input::get('verifiedby'); 
				$inventory_responsible3->keterangan = 'verifiedby';  
				$inventory_responsible3->save(); 
			}
			if(Input::get('approvedby')){
				$inventory_responsible4 = new Requestresponsible;  
				$inventory_responsible4->id_inv = $last_id;
				$inventory_responsible4->id_employee = Input::get('approvedby'); 
				$inventory_responsible4->keterangan = 'approvedby';  
				$inventory_responsible4->save(); 
			}
 
			// Insert in Table Request Product
			$good_id = Input::get('good_id');
			$jml_id = Input::get('jml_id');
			$price_id = Input::get('price_id');

			for($j=0;$j<count($good_id);$j++){ 
				$inventory_product = new Requestproduct; 
				$inventory_product->id_inv = $last_id;
				$inventory_product->id_brg = $good_id[$j];
				$inventory_product->quantity = $jml_id[$j];
				$inventory_product->sub_total = $price_id[$j]; 
				$inventory_product->save();   
			}

  		    Session::flash('message', 'The records are inserted successfully');	
			return Redirect::to('inventories/roindex');	 
		}
		
	}
 
	public function getRodelete($id){

		$rr = Requestresponsible::where('id_inv',$id);
		$rr->delete();

		$rp = Requestproduct::where('id_inv',$id);
		$rp->delete();
 
		$inventory = Inventories::find($id);
		$inventory->delete();

		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('inventories/roindex');
		
	}

	public function getRoedit($id){
	 
		$data = Inventories::find($id); 

		$inventory_product = DB::table('request_product')
            ->join('goods', 'request_product.id_brg', '=', 'goods.id') 
            ->select('request_product.id_brg', 'goods.nama_barang','request_product.sub_total','request_product.quantity')
            ->where('request_product.id_inv','=',$id) 
            ->get();

		$madeby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%madeby%')
            ->get();

		$requestby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%requestby%')
            ->get();

		$verifiedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%verifiedby%')
            ->get();

		$approvedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%approvedby%')
            ->get();

		$states = DB::table('state_trx')
					->whereIn('id', [1, 2])
					->get();
 
		$options = array(
			'data'=>$data,
			'inventory_product'=>$inventory_product,
			'madeby'=>$madeby,
			'requestby'=>$requestby,
			'verifiedby'=>$verifiedby,
			'approvedby'=>$approvedby,
			'states'=>$states,
		);
		return View::make('home/dashboard',array())->nest('content', 'inventories/roedit',$options);
	} 
	// END FUNGSI Request Order //

	// FUNGSI ApprovalRO //
	public function getApproindex(){
		$key = Input::get('search');
		if(isset($key)){
			// $data = Inventories::where('type_trx','=', '1','nomer_order', 'like', '%'.$key.'%')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('request')
            ->join('state_trx', 'state_trx.id', '=', 'request.state') 
            ->join('type_trx', 'type_trx.id', '=', 'request.type_trx') 
            ->select('request.id', 'request.nomer_order','request.departement','request.keperluan','request.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('request.type_trx','=','1') 
            ->where('request.nomer_order','like','%'.$key.'%') 
            ->where('request.state','=','2') 
            ->orderBy('request.nomer_order', 'desc')->paginate(10);
		}else{
			// $data = Inventories::where('type_trx','=', '1')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('request')
            ->join('state_trx', 'state_trx.id', '=', 'request.state') 
            ->join('type_trx', 'type_trx.id', '=', 'request.type_trx') 
            ->select('request.id', 'request.nomer_order','request.departement','request.keperluan','request.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('request.type_trx','=','1')  
            ->where('request.state','=','2') 
            ->orderBy('request.nomer_order', 'desc')->paginate(10);
		} 
		return View::make('home/dashboard',array())->nest('content', 'inventories/approindex',array('data'=>$data));
	}

	public function getSaveappreq(){

		$id = Input::get('id');
		$year = date('Y');
		$month = date('m');
		$date = date('d');
		$now = date('Y-m-d');

		// Fungsi Update 
		if($id){    
			if(Input::get('state') == '3'){
				$type_inv = 2;
			}else{
				$type_inv = 1;
			}
 
			$inventory = Inventories::find($id);
			$inventory->nomer_order = Input::get('nomer_order');
			$inventory->departement = Input::get('departement');
			$inventory->keperluan = Input::get('keperluan');
			$inventory->urgentity = Input::get('urgentity');
			$inventory->justification = Input::get('justification'); 
			$inventory->state = Input::get('state'); 
			$inventory->type_trx = $type_inv;
			$inventory->tgl_in_trx = $now;
			$inventory->tgl_out_trx = $now; 
			$inventory->save();

			$bs = Requestresponsible::where('id_inv',$id);
			$bs->delete();
			$cek_bs = Requestresponsible::where('id_inv','=',$id)->first();
			if(empty($cek_bs)){
				if(Input::get('madeby')){
				$inventory_responsible1 = new Requestresponsible;  
				$inventory_responsible1->id_inv = $id;
				$inventory_responsible1->id_employee = Input::get('madeby');   
				$inventory_responsible1->keterangan = 'madeby';  
				$inventory_responsible1->save(); 	
				}
				if(Input::get('requestby')){
					$inventory_responsible2 = new Requestresponsible;  
					$inventory_responsible2->id_inv = $id;
					$inventory_responsible2->id_employee = Input::get('requestby');  
					$inventory_responsible2->keterangan = 'requestby';  
					$inventory_responsible2->save(); 
				}
				if(Input::get('verifiedby')){
					$inventory_responsible3 = new Requestresponsible;  
					$inventory_responsible3->id_inv = $id;
					$inventory_responsible3->id_employee = Input::get('verifiedby'); 
					$inventory_responsible3->keterangan = 'verifiedby';  
					$inventory_responsible3->save(); 
				}
				if(Input::get('approvedby')){
					$inventory_responsible4 = new Requestresponsible;  
					$inventory_responsible4->id_inv = $id;
					$inventory_responsible4->id_employee = Input::get('approvedby'); 
					$inventory_responsible4->keterangan = 'approvedby';  
					$inventory_responsible4->save(); 
				}
			} 

			// $rp = Requestproduct::where('id_inv',$id);
			// $del_prod = $rp->delete(); 
			// if(!empty($del_prod)){
			// 	$cek_rp = Requestproduct::where('id_inv','=',$id)->first();
			// 	if(empty($cek_rp)){
			$good_id = Input::get('good_id');
			$jml_id = Input::get('jml_id');
			$price_id = Input::get('price_id');

			for($j=0;$j<count($good_id);$j++){ 
				// $inventory_product = new Requestproduct; 
				$inventory_product = Requestproduct::find($id);  
				$inventory_product->id_brg = $good_id[$j];
				$inventory_product->quantity = $jml_id[$j];
				$inventory_product->sub_total = $price_id[$j]; 
				$inventory_product->save();   
			}
			// 	}
			// }
			

			// Begin insert to Purchase table //
			if(Input::get('state') == '3'){
				$data = Purchases::orderBy('id', 'desc')->first();

				if(isset($data->id)){
					$index = $data->id+1;
				}else{
					$index = 1;
				}

				$x = strlen($index);
				$y = null;
				for($i=4;$i>$x;$i--){
					$y.= '0';
				}
				$y.=$index; 
				$purchase_code = 'PO-'.''.$year.''.$month.''.$date.''.$y;

				$purchase = new Purchases;
				$purchase->nomer_order_req = Input::get('nomer_order');
				$purchase->nomer_order_purc = $purchase_code;
				$purchase->departement = Input::get('departement');
				$purchase->keperluan = Input::get('keperluan');
				$purchase->urgentity = Input::get('urgentity');
				$purchase->justification = Input::get('justification');
				$purchase->state = Input::get('state');
				$purchase->type_trx = 2;
				$purchase->purchase_date = $now;
				$purchase->tgl_in_trx = $now;
				$purchase->tgl_out_trx = $now; 
				$purchase->save();
			} 
 			// End insert to Purchase table //

		   Session::flash('message', 'The records are updated successfully');	
		   return Redirect::to('inventories/approindex');	  
		} 
	}
 
	public function getRoappdelete($id){

		$rr = Requestresponsible::where('id_inv',$id);
		$rr->delete();

		$rp = Requestproduct::where('id_inv',$id);
		$rp->delete();
 
		$inventory = Inventories::find($id);
		$inventory->delete();

		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('inventories/approindex');
		
	}

	public function getRoappedit($id){
	 
		$data = Inventories::find($id); 

		$inventory_product = DB::table('request_product')
            ->join('goods', 'request_product.id_brg', '=', 'goods.id') 
            ->select('request_product.id_brg', 'goods.nama_barang','request_product.sub_total','request_product.quantity')
            ->where('request_product.id_inv','=',$id) 
            ->get();

		$madeby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%madeby%')
            ->get();

		$requestby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%requestby%')
            ->get();

		$verifiedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%verifiedby%')
            ->get();

		$approvedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('request_responsible.id_inv','=',$id)
            ->where('request_responsible.keterangan', 'like', '%approvedby%')
            ->get();

		$states = DB::table('state_trx')
					->whereIn('id', [3, 4])
					->get();
 
		$options = array(
			'data'=>$data,
			'inventory_product'=>$inventory_product,
			'madeby'=>$madeby,
			'requestby'=>$requestby,
			'verifiedby'=>$verifiedby,
			'approvedby'=>$approvedby,
			'states'=>$states,
		);
		return View::make('home/dashboard',array())->nest('content', 'inventories/approedit',$options);
	} 
	// END FUNGSI ApprovalRO //

	// FUNGSI Purchase Order // 
	public function getPoindex(){
		$key = Input::get('search');
		if(isset($key)){
			// $data = Inventories::where('type_trx','=', '1','nomer_order', 'like', '%'.$key.'%')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('purchase')
            ->join('state_trx', 'state_trx.id', '=', 'purchase.state') 
            ->join('type_trx', 'type_trx.id', '=', 'purchase.type_trx') 
            ->select('purchase.id', 'purchase.nomer_order_req','purchase.nomer_order_purc','purchase.departement','purchase.keperluan','purchase.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('purchase.type_trx','=','2') 
            ->where('purchase.nomer_order_purc','like','%'.$key.'%') 
            ->where('purchase.state','=','3') 
            ->orderBy('purchase.nomer_order_purc', 'desc')->paginate(10);
		}else{
			// $data = Inventories::where('type_trx','=', '1')->orderBy('nomer_order', 'desc')->paginate(10);
			$data = DB::table('purchase')
            ->join('state_trx', 'state_trx.id', '=', 'purchase.state') 
            ->join('type_trx', 'type_trx.id', '=', 'purchase.type_trx') 
            ->select('purchase.id', 'purchase.nomer_order_req','purchase.nomer_order_purc','purchase.departement','purchase.keperluan','purchase.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('purchase.type_trx','=','2')  
            ->where('purchase.state','=','3') 
            ->orderBy('purchase.nomer_order_purc', 'desc')->paginate(10);
		} 
		return View::make('home/dashboard',array())->nest('content', 'inventories/poindex',array('data'=>$data));
	} 
 
	public function getPoedit($id){ 
		$data = Purchases::find($id); 

		$inventory_product = DB::table('request_product')
            ->join('goods', 'request_product.id_brg', '=', 'goods.id') 
            ->join('request', 'request_product.id_inv', '=', 'request.id') 
            ->join('purchase', 'request.nomer_order', '=', 'purchase.nomer_order_req') 
            ->select('request_product.id_brg', 'goods.nama_barang','request_product.sub_total','request_product.quantity')
            ->where('purchase.id','=',$id) 
            ->get();
 
		$madeby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->join('request', 'request_responsible.id_inv', '=', 'request.id') 
            ->join('purchase', 'request.nomer_order', '=', 'purchase.nomer_order_req') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('purchase.id','=',$id)
            ->where('request_responsible.keterangan', 'like', '%madeby%')
            ->get();

		$requestby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->join('request', 'request_responsible.id_inv', '=', 'request.id') 
            ->join('purchase', 'request.nomer_order', '=', 'purchase.nomer_order_req') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('purchase.id','=',$id)
            ->where('request_responsible.keterangan', 'like', '%requestby%')
            ->get();

		$verifiedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->join('request', 'request_responsible.id_inv', '=', 'request.id') 
            ->join('purchase', 'request.nomer_order', '=', 'purchase.nomer_order_req') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('purchase.id','=',$id)
            ->where('request_responsible.keterangan', 'like', '%verifiedby%')
            ->get();

		$approvedby = DB::table('request_responsible')
            ->join('employee', 'request_responsible.id_employee', '=', 'employee.id') 
            ->join('request', 'request_responsible.id_inv', '=', 'request.id') 
            ->join('purchase', 'request.nomer_order', '=', 'purchase.nomer_order_req') 
            ->select('request_responsible.id_employee', 'employee.full_name')
            ->where('purchase.id','=',$id)
            ->where('request_responsible.keterangan', 'like', '%approvedby%')
            ->get();

		$states = DB::table('state_trx')
					->whereIn('id', [2, 4])
					->get();
 
		$options = array(
			'data'=>$data,
			'inventory_product'=>$inventory_product,
			'madeby'=>$madeby,
			'requestby'=>$requestby,
			'verifiedby'=>$verifiedby,
			'approvedby'=>$approvedby,
			'states'=>$states,
		);
		return View::make('home/dashboard',array())->nest('content', 'inventories/poedit',$options);
	} 

	public function getSavepo(){

		$id = Input::get('id');
		$year = date('Y');
		$month = date('m');
		$date = date('d');
		$now = date('Y-m-d');

		// Fungsi Update 
		if($id){  
		  
			if(Input::get('state') == '2'){
				$type_inv = 2;
				$state = 2;
			}else{
				$type_inv = 1;
				$state = 2;
			}
 
			$inventory = Purchases::find($id);  
			$inventory->state = $state; 
			$inventory->type_trx = $type_inv;
			$inventory->tgl_in_trx = $now;
			$inventory->tgl_out_trx = $now; 
			$inventory->save();

			// Insert in Table Request Resposible
			if(Input::get('madeby')){
				$inventory_responsible1 = new Purchaseresponsible;  
				$inventory_responsible1->id_inv = $id;
				$inventory_responsible1->id_employee = Input::get('madeby');   
				$inventory_responsible1->keterangan = 'madeby';  
				$inventory_responsible1->save(); 	
			}
			if(Input::get('requestby')){
				$inventory_responsible2 = new Purchaseresponsible;  
				$inventory_responsible2->id_inv = $id;
				$inventory_responsible2->id_employee = Input::get('requestby');  
				$inventory_responsible2->keterangan = 'requestby';  
				$inventory_responsible2->save(); 
			}
			if(Input::get('verifiedby')){
				$inventory_responsible3 = new Purchaseresponsible;  
				$inventory_responsible3->id_inv = $id;
				$inventory_responsible3->id_employee = Input::get('verifiedby'); 
				$inventory_responsible3->keterangan = 'verifiedby';  
				$inventory_responsible3->save(); 
			}
			if(Input::get('approvedby')){
				$inventory_responsible4 = new Purchaseresponsible;  
				$inventory_responsible4->id_inv = $id;
				$inventory_responsible4->id_employee = Input::get('approvedby'); 
				$inventory_responsible4->keterangan = 'approvedby';  
				$inventory_responsible4->save(); 
			}
 
			// Insert in Table Purchaseproduct Product
			$good_id = Input::get('good_id');
			$jml_id = Input::get('jml_id');
			$price_id = Input::get('price_id');

			for($j=0;$j<count($good_id);$j++){ 
				$inventory_product = new Purchaseproduct; 
				$inventory_product->id_inv = $id;
				$inventory_product->id_brg = $good_id[$j];
				$inventory_product->quantity = $jml_id[$j];
				$inventory_product->sub_total = $price_id[$j]; 
				$inventory_product->save();   
			}

		   Session::flash('message', 'The records are updated successfully');	
		   return Redirect::to('inventories/poindex');	  
		} 
	}
 
	public function getPodelete($id){

		$rr = Purchaseresponsible::where('id_inv',$id);
		$rr->delete();

		$rp = Purchaseproduct::where('id_inv',$id);
		$rp->delete();
 
		$inventory = Purchases::find($id);
		$inventory->delete();

		Session::flash('message', 'The records are deleted successfully');
		return Redirect::to('inventories/poindex');
		
	} 
	// END FUNGSI Purchase Order //


	// FUNGSI ApprovalPO //
	public function getApppoindex(){
		$key = Input::get('search');
		if(isset($key)){ 
			$data = DB::table('purchase')
            ->join('state_trx', 'state_trx.id', '=', 'purchase.state') 
            ->join('type_trx', 'type_trx.id', '=', 'purchase.type_trx') 
            ->select('purchase.id', 'purchase.nomer_order_purc','purchase.departement','purchase.keperluan','purchase.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('purchase.type_trx','=','2') 
            ->where('purchase.nomer_order_purc','like','%'.$key.'%') 
            ->where('purchase.state','=','2') 
            ->orderBy('purchase.nomer_order_purc', 'desc')->paginate(10);
		}else{ 
			$data = DB::table('purchase')
            ->join('state_trx', 'state_trx.id', '=', 'purchase.state') 
            ->join('type_trx', 'type_trx.id', '=', 'purchase.type_trx') 
            ->select('purchase.id', 'purchase.nomer_order_purc','purchase.departement','purchase.keperluan','purchase.urgentity','state_trx.state_name','type_trx.nama_type')
            ->where('purchase.type_trx','=','2')  
            ->where('purchase.state','=','2') 
            ->orderBy('purchase.nomer_order_purc', 'desc')->paginate(10);
		} 
		return View::make('home/dashboard',array())->nest('content', 'inventories/apppoindex',array('data'=>$data));
	}

	public function getPoappedit($id){ 
		$data = Purchases::find($id); 

		$inventory_product = DB::table('purchase_product')
            ->leftJoin('supplier_goods', 'purchase_product.id_brg', '=', 'supplier_goods.id') 
            ->join('goods', 'supplier_goods.id_goods', '=', 'goods.id') 
            ->join('supplier', 'supplier_goods.id_supplier', '=', 'supplier.id') 
            ->select('purchase_product.id_brg', DB::raw('CONCAT(goods.nama_barang,"- Suuplier ",supplier.nama_toko) as nama_barang') ,'purchase_product.sub_total','purchase_product.quantity')
            ->where('purchase_product.id_inv','=',$id) 
            ->orderBy('purchase_product.id_brg', 'desc')->paginate(10);

          // print_r($inventory_product);
          // exit;

		$madeby = DB::table('purchase_responsible')
            ->join('employee', 'purchase_responsible.id_employee', '=', 'employee.id') 
            ->select('purchase_responsible.id_employee', 'employee.full_name')
            ->where('purchase_responsible.id_inv','=',$id)
            ->where('purchase_responsible.keterangan', 'like', '%madeby%')
            ->get();

		$requestby = DB::table('purchase_responsible')
            ->join('employee', 'purchase_responsible.id_employee', '=', 'employee.id') 
            ->select('purchase_responsible.id_employee', 'employee.full_name')
            ->where('purchase_responsible.id_inv','=',$id)
            ->where('purchase_responsible.keterangan', 'like', '%requestby%')
            ->get();

		$verifiedby = DB::table('purchase_responsible')
            ->join('employee', 'purchase_responsible.id_employee', '=', 'employee.id') 
            ->select('purchase_responsible.id_employee', 'employee.full_name')
            ->where('purchase_responsible.id_inv','=',$id)
            ->where('purchase_responsible.keterangan', 'like', '%verifiedby%')
            ->get();

		$approvedby = DB::table('purchase_responsible')
            ->join('employee', 'purchase_responsible.id_employee', '=', 'employee.id') 
            ->select('purchase_responsible.id_employee', 'employee.full_name')
            ->where('purchase_responsible.id_inv','=',$id)
            ->where('purchase_responsible.keterangan', 'like', '%approvedby%')
            ->get();

		$states = DB::table('state_trx')
					->whereIn('id', [6, 4])
					->orderBy('state_trx.id', 'desc')
					->get();
 
		$options = array(
			'data'=>$data,
			'inventory_product'=>$inventory_product,
			'madeby'=>$madeby,
			'requestby'=>$requestby,
			'verifiedby'=>$verifiedby,
			'approvedby'=>$approvedby,
			'states'=>$states,
		);
		return View::make('home/dashboard',array())->nest('content', 'inventories/apppoedit',$options);
	} 
	// public function getSaveappreq(){

	// 	$id = Input::get('id');
	// 	$year = date('Y');
	// 	$month = date('m');
	// 	$date = date('d');
	// 	$now = date('Y-m-d');

	// 	// Fungsi Update 
	// 	if($id){  
	// 		$bs = Requestresponsible::where('id_inv',$id);
	// 		$bs->delete();

	// 		$br = Requestproduct::where('id_inv',$id);
	// 		$br->delete();

	// 		if(Input::get('state') == '3'){
	// 			$type_inv = 2;
	// 		}else{
	// 			$type_inv = 1;
	// 		}
 
	// 		$inventory = Inventories::find($id);
	// 		$inventory->nomer_order = Input::get('nomer_order');
	// 		$inventory->departement = Input::get('departement');
	// 		$inventory->keperluan = Input::get('keperluan');
	// 		$inventory->urgentity = Input::get('urgentity');
	// 		$inventory->justification = Input::get('justification'); 
	// 		$inventory->state = Input::get('state'); 
	// 		$inventory->type_trx = $type_inv;
	// 		$inventory->tgl_in_trx = $now;
	// 		$inventory->tgl_out_trx = $now; 
	// 		$inventory->save();

	// 		$cek_bs = Requestresponsible::where('id_inv','=',$id)->first();
	// 		if(empty($cek_bs)){
	// 			if(Input::get('madeby')){
	// 			$inventory_responsible1 = new Requestresponsible;  
	// 			$inventory_responsible1->id_inv = $id;
	// 			$inventory_responsible1->id_employee = Input::get('madeby');   
	// 			$inventory_responsible1->keterangan = 'madeby';  
	// 			$inventory_responsible1->save(); 	
	// 			}
	// 			if(Input::get('requestby')){
	// 				$inventory_responsible2 = new Requestresponsible;  
	// 				$inventory_responsible2->id_inv = $id;
	// 				$inventory_responsible2->id_employee = Input::get('requestby');  
	// 				$inventory_responsible2->keterangan = 'requestby';  
	// 				$inventory_responsible2->save(); 
	// 			}
	// 			if(Input::get('verifiedby')){
	// 				$inventory_responsible3 = new Requestresponsible;  
	// 				$inventory_responsible3->id_inv = $id;
	// 				$inventory_responsible3->id_employee = Input::get('verifiedby'); 
	// 				$inventory_responsible3->keterangan = 'verifiedby';  
	// 				$inventory_responsible3->save(); 
	// 			}
	// 			if(Input::get('approvedby')){
	// 				$inventory_responsible4 = new Requestresponsible;  
	// 				$inventory_responsible4->id_inv = $id;
	// 				$inventory_responsible4->id_employee = Input::get('approvedby'); 
	// 				$inventory_responsible4->keterangan = 'approvedby';  
	// 				$inventory_responsible4->save(); 
	// 			}
	// 		} 

	// 		$cek_rp = Requestproduct::where('id_inv','=',$id)->first();
	// 		if(empty($cek_rp)){
	// 			$good_id = Input::get('good_id');
	// 			$jml_id = Input::get('jml_id');
	// 			$price_id = Input::get('price_id');

	// 			for($j=0;$j<count($good_id);$j++){ 
	// 				$inventory_product = new Requestproduct; 
	// 				$inventory_product->id_inv = $id;
	// 				$inventory_product->id_brg = $good_id[$j];
	// 				$inventory_product->quantity = $jml_id[$j];
	// 				$inventory_product->sub_total = $price_id[$j]; 
	// 				$inventory_product->save();   
	// 			}
	// 		}

	// 		// Begin insert to Purchase table //
	// 		if(Input::get('state') == '3'){
	// 			$data = Purchases::orderBy('id', 'desc')->first();

	// 			if(isset($data->id)){
	// 				$index = $data->id+1;
	// 			}else{
	// 				$index = 1;
	// 			}

	// 			$x = strlen($index);
	// 			$y = null;
	// 			for($i=4;$i>$x;$i--){
	// 				$y.= '0';
	// 			}
	// 			$y.=$index; 
	// 			$purchase_code = 'PO-'.''.$year.''.$month.''.$date.''.$y;

	// 			$purchase = new Purchases;
	// 			$purchase->nomer_order_req = Input::get('nomer_order');
	// 			$purchase->nomer_order_purc = $purchase_code;
	// 			$purchase->departement = Input::get('departement');
	// 			$purchase->keperluan = Input::get('keperluan');
	// 			$purchase->urgentity = Input::get('urgentity');
	// 			$purchase->justification = Input::get('justification');
	// 			$purchase->state = Input::get('state');
	// 			$purchase->type_trx = 2;
	// 			$purchase->purchase_date = $now;
	// 			$purchase->tgl_in_trx = $now;
	// 			$purchase->tgl_out_trx = $now; 
	// 			$purchase->save();
	// 		} 
 // 			// End insert to Purchase table //

	// 	   Session::flash('message', 'The records are updated successfully');	
	// 	   return Redirect::to('inventories/approindex');	  
	// 	} 
	// }
 
	// public function getRoappdelete($id){

	// 	$rr = Requestresponsible::where('id_inv',$id);
	// 	$rr->delete();

	// 	$rp = Requestproduct::where('id_inv',$id);
	// 	$rp->delete();
 
	// 	$inventory = Inventories::find($id);
	// 	$inventory->delete();

	// 	Session::flash('message', 'The records are deleted successfully');
	// 	return Redirect::to('inventories/approindex');
		
	// }

	// END FUNGSI ApprovalRO //

	 
	public function getUrgentity(){
		$q = Input::get('q');
		$data = Urgentity::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();
		$array = array();
		foreach($data as $row){
			$array[] = array(
				'id'=>$row->id,
				'text'=>$row->name
			);
		}
		echo json_encode($array);
	}

	public function getResponsible(){
		$q = Input::get('q');
		$data = Employee::where('full_name', 'like', '%'.$q.'%')->orderBy('full_name', 'asc')->limit(10)->get();
		$array = array();
		foreach($data as $row){
			$array[] = array(
				'id'=>$row->id,
				'text'=>$row->full_name
			);
		}
		echo json_encode($array);
	}

	public function getGoods(){
		$q = Input::get('q');
		$data = Goods::where('nama_barang', 'like', '%'.$q.'%')->orderBy('nama_barang', 'asc')->limit(10)->get();
		$array = array();
		foreach($data as $row){
			$array[] = array(
				'id'=>$row->id,
				'text'=>$row->nama_barang
			);
		}
		echo json_encode($array);
	}
 
	public function getSuppliergoods(){

		$q = Input::get('q');

		$data = DB::table('supplier_goods')
            ->join('supplier', 'supplier_goods.id_supplier', '=', 'supplier.id') 
            ->join('goods', 'supplier_goods.id_goods', '=', 'goods.id') 
            ->select('supplier_goods.id', DB::raw('CONCAT(goods.nama_barang,"- Suuplier ",supplier.nama_toko) as full_name')) 
            ->where('goods.nama_barang', 'like', '%'.$q.'%')
            ->get();

		// $data = Goods::where('nama_barang', 'like', '%'.$q.'%')->orderBy('nama_barang', 'asc')->limit(10)->get();

		$array = array();
		foreach($data as $row){
			$array[] = array(
				'id'=>$row->id,
				'text'=>$row->full_name
			);
		}
		echo json_encode($array);
	}
 
	public function getPrice($id){
		$data = Goods::find($id);
		$array[] = array(
			'id'=>$data->id,
			'price'=>$data->harga_beli
		);
		echo json_encode($array);
	}
 
	public function getPricesupplier($id){
		$data = Pricesupplier::find($id);
 
		$array[] = array(
			'id'=>$data->id,
			'price'=>$data->harga_beli
		);
		echo json_encode($array);
	}
 

	public function getReport(){
		$first_date = Input::get('first_date');
		$last_date = Input::get('last_date');
		$submit =  Input::get('submit');
		$page = Input::get('page');
		if(isset($first_date) && isset($last_date)){
			$data = Bookings::where('date_booking','>=',$first_date)->where('date_booking','<=',$last_date)->paginate(10);
		}else{
			$data = Bookings::orderBy('id', 'desc')->paginate(10);
		}
		if(isset($submit) && $submit=='print'){

			if(isset($page)){
				$no = ($page*10)-9;
			}else{
				$no = 1;
			}

			$pdf = App::make('dompdf');
			$html = '<center><b>Report of Bookings</b></center>';
			$html .= '<br><br><br>';
			$html .= '<table border="1" align="center" width="100%" padding="0" cellpadding="5">';
			$html .= '<tr>';
				$html .= '<td>No</td>';
				$html .= '<td>Booking Code</td>';
				$html .= '<td>Date Booking</td>';
				$html .= '<td>Date Expired</td>';
				$html .= '<td>Service Count</td>';
				$html .= '<td>Room Count</td>';
			$html .= '</tr>';
			foreach($data as $row){
				$html .= '<tr>';
					$html .= '<td>'.$no.'</td>';
					$html .= '<td>'.$row->booking_code.'</td>';
					$html .= '<td>'.$row->date_booking.'</td>';
					$html .= '<td>'.$row->date_booking_to.'</td>';
					$html .= '<td>'.CountService($row->id).'</td>';
					$html .= '<td>'.CountRoom($row->id).'</td>';
				$html .= '</tr>';
				$no++;
			}
			$html .= '</table>';

			$pdf->loadHTML($html)->setPaper('a4')->setOrientation('potrait');
			return $pdf->download('Report Of Booking.pdf');

		}else{
			return View::make('home/dashboard',array())->nest('content', 'bookings/report',array('data'=>$data));
		}
	}

	public function getPayments(){
		$first_date = Input::get('first_date');
		$last_date = Input::get('last_date');
		$submit =  Input::get('submit');
		$page = Input::get('page');
		if(isset($first_date) && isset($last_date)){
			$data = Bookings::where('date_booking','>=',$first_date)->where('date_booking','<=',$last_date)->paginate(10);
		}else{
			$data = Bookings::orderBy('id', 'desc')->paginate(10);
		}
		if(isset($submit) && $submit=='print'){

			if(isset($page)){
				$no = ($page*10)-9;
			}else{
				$no = 1;
			}

			$pdf = App::make('dompdf');
			$html = '<center><b>Report of Financial</b></center>';
			$html .= '<br><br><br>';
			$html .= '<table border="1" align="center" width="100%" padding="0" cellpadding="5">';
			$html .= '<tr>';
				$html .= '<td>No</td>';
				$html .= '<td>Booking Code</td>';
				$html .= '<td>Date Booking</td>';
				$html .= '<td>Date Expired</td>';
				$html .= '<td>Service & Room Count</td>';
				$html .= '<td>Total</td>';
			$html .= '</tr>';
			foreach($data as $row){
				$temp = CountService($row->id)+CountRoom($row->id);
				$html .= '<tr>';
					$html .= '<td>'.$no.'</td>';
					$html .= '<td>'.$row->booking_code.'</td>';
					$html .= '<td>'.$row->date_booking.'</td>';
					$html .= '<td>'.$row->date_booking_to.'</td>';
					$html .= '<td>'.$temp.'</td>';
					$html .= '<td>'.Total($row->id).'</td>';
				$html .= '</tr>';
				$no++;
			}
			$html .= '</table>';
			
			$pdf->loadHTML($html)->setPaper('a4')->setOrientation('potrait');
			return $pdf->download('Report Of Financial.pdf');

		}else{
			return View::make('home/dashboard',array())->nest('content', 'payments/report',array('data'=>$data));
		}
		
	}

}