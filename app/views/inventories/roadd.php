<div class="container-fluid">
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<div class="panel-body">
			<div class="row">
				<div class="pull-right">
					<a href="<?php echo url();?>/inventories/index" data-toggle="tooltip" title="Back To Grid" class="btn btn-default"><i class="fa fa-refresh"></i> Back</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<div class="panel panel-default">
			<div class="panel-heading"> 
				<h3 class="panel-title">Request Order</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" method="POST" action="<?php echo url();?>/inventories">
					<input type="hidden" name="id" class="form-control" />  
					<div class="form-group required">
						<label class="col-sm-2 control-label">State Order</label>
						<div class="col-sm-6">
							<select name="state" class="form-control" required> 
								<option value="2">Posting for Approval</option> 
								<option value="1">Draft</option> 
							</select>
						</div>
					</div>
					<div class="form-group required">
						<label class="col-sm-2 control-label">Department</label>
						<div class="col-sm-6">
							<input type="text" name="departement" class="form-control" required/>
						</div>
					</div>
					<div class="form-group required">
						<label class="col-sm-2 control-label">Purposes</label>
						<div class="col-sm-6">
							<input type="text" name="keperluan" class="form-control" required/>
						</div>
					</div>
					<div class="form-group required" >
						 <label class="col-sm-2 control-label">Class of Urgently</label>
						 <div class="col-sm-10">
							<label class="radio-inline">
								 <input type="radio" name="urgentity" value="Top Urgen" required/> Top Urgen
							</label>
							<label class="radio-inline">
								 <input type="radio" name="urgentity" value="Urgen" required/> Urgen
							</label>
							<label class="radio-inline">
								 <input type="radio" name="urgentity" value="Biasa" required/> Biasa
							</label>
							<label class="radio-inline">
								 <input type="radio" name="urgentity" value="EDD" required/> EDD
							</label>
						 </div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Justification</label>
						<div class="col-sm-6"> 
							<textarea class="form-control" name="justification" rows="2"></textarea>
						</div>
					</div>
					<div class="form-group required">
						 <label class="col-sm-2 control-label">Made By</label>
						 <div class="col-sm-4">
							 <input type="hidden" id="madeby" name="madeby" class="form-control"  required/>
						 </div>
					</div>
					<div class="form-group required">
						 <label class="col-sm-2 control-label">Request By</label>
						 <div class="col-sm-4">
							 <input type="hidden" id="requestby" name="requestby" class="form-control"  required/>
						 </div>
					</div>
					<div class="form-group required"> 
						 <label class="col-sm-2 control-label">Verified By</label>
						 <div class="col-sm-4">
							 <input type="hidden" id="verifiedby" name="verifiedby" class="form-control"  required/>
						 </div>
					</div>
					<div class="form-group required">
						 <label class="col-sm-2 control-label">Approved By</label>
						 <div class="col-sm-4">
							 <input type="hidden" id="approvedby" name="approvedby" class="form-control"  required/>
						 </div>
					</div> 
					<div id="data-service"></div>
					<div class="table-responsive">
						<table class="table table-bordered table-hover" id="TData">
							<thead>
								<tr>
									<td colspan="5">
										<label># Details Of Good</label>
										<a href="javascript:void(0)" onclick="AddGoods()" title="Add New Goods" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Goods</a>
									</td>
								</tr>
								<tr> 
									<th>Quantity</th> 
									<th>Name Goods</th>
									<th>Price</th> 
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="pull-right">
						<button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
						<button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
 

        $('#madeby').select2({
            ajax: {
                placeholder: 'Type Made By',    
                url: '<?php echo url();?>/inventories/responsible',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });

        $('#requestby').select2({
            ajax: {
                placeholder: 'Type Request By',    
                url: '<?php echo url();?>/inventories/responsible',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });

        $('#verifiedby').select2({
            ajax: {
                placeholder: 'Type Verified By',    
                url: '<?php echo url();?>/inventories/responsible',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });

        $('#approvedby').select2({
            ajax: {
                placeholder: 'Type Approved By',    
                url: '<?php echo url();?>/inventories/responsible',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });

         
	});

	
	function AddGoods(){
		var row = $('#TData tbody tr').length;
		if(row>0){
			var index = $('.index').last().attr('id');
			index = parseInt(index);
			row = parseInt(row);
			row = index+1;
		}
		var html = '<tr id="'+row+'" class="index">'; 
		html += '<td><input type="text" id="number'+row+'" name="jml_id[]" class="form-control"  required/></td>'; 
		html += '<td><input type="hidden" id="type_id'+row+'" name="good_id[]" class="form-control" onChange="javascript:price(this)" required/></td>';
		html += '<td><input type="text" id="price'+row+'" name="price_id[]" class="form-control" readonly="true" required/></td>';
		html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>';
		html += '</tr>';
		$('#TData tbody').append(html);
		$('#type_id'+row).select2({
            ajax: {
                placeholder: 'Type Of Goods',    
                url: '<?php echo url();?>/inventories/goods',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });
      
	}
 
	function price(x){
		var id = $(x).attr('id');
		var value = $('#'+id).val();
		var json = LoadPrice(value);
		var data = eval(json);
		var index = id.substr(7); 
		var jml = $("#number"+index).val(); 

		if(jml < 1){
			$("#price"+index).val('');
		}else{ 
			var sub_tot = jml * data[0].price;
			$("#price"+index).val(sub_tot);
		}
	}
 
	function LoadPrice(id){
		var value = '';
		$.ajax({
			url: '<?php echo url();?>/inventories/price/'+id,
			dataType: 'json',
			async: false,
			success:function(data){
				value = data;
			}
		});
		return value;
	}
 
	function deleteRow(btn) {
	  var row = btn.parentNode.parentNode;
	  row.parentNode.removeChild(row);
	}

	AddGoods();

</script>