<!DOCTYPE html>
<html dir="ltr" lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>i-Groot | Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
		 <script type="text/javascript">
  			  var base_url = '<?php echo url();?>/';
  		</script>
		<script type="text/javascript" src="<?php echo url();?>/assets/js/jquery/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="<?php echo url();?>/assets/js/bootstrap/js/bootstrap.min.js"></script>
		<link href="<?php echo url();?>/assets/js/bootstrap/less/bootstrap.less" rel="stylesheet/less" />
		<script src="<?php echo url();?>/assets/js/bootstrap/less-1.7.4.min.js"></script>
		<link href="<?php echo url();?>/assets/js/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
		<link href="<?php echo url();?>/assets/js/summernote/summernote.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php echo url();?>/assets/js/summernote/summernote.js"></script>
		<script src="<?php echo url();?>/assets/js/jquery/datetimepicker/moment.js" type="text/javascript"></script>
		<script src="<?php echo url();?>/assets/js/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<link href="<?php echo url();?>/assets/js/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
		<link type="text/css" href="<?php echo url();?>/assets/css/stylesheet.css" rel="stylesheet" media="screen" />
		<link type="text/css" href="<?php echo url();?>/assets/css/select2.css" rel="stylesheet" media="screen" />
		<link type="text/css" href="<?php echo url();?>/assets/css/select2-bootstrap.css" rel="stylesheet" media="screen" />
		<script src="<?php echo url();?>/assets/js/jquery/select2.js" type="text/javascript"></script>
		<script src="<?php echo url();?>/assets/js/jquery/date.js" type="text/javascript"></script>
		<script src="<?php echo url();?>/assets/js/common.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="container">
			<header id="header" class="navbar navbar-static-top">
				<div class="navbar-header">
					<a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
				</div>
				<ul class="nav pull-right">
					<li>
						<a href=" "><span class="hidden-xs hidden-sm hidden-md">Profile</span> <i class="fa fa-user fa-lg"></i></a>
					</li>
					<li>
						<a href="<?php echo url();?>/logout"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a>
					</li>
				</ul>
			</header>
			<nav id="column-left">
				<div id="profile">
					<div>
						<a class="dropdown-toggle" data-toggle="dropdown"></a>
					</div>
					<div> 
						<h4> <small style="color: white">Hello,</small> <?php echo strtoupper(Session::get('username')); ?></h4> 
					</div>
				</div>
				<ul id="menu">
					<li id="dashboard">
						<a href="<?php echo url();?>/dashboard"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
					</li>
					<li>
						<a class="parent"><i class="fa fa-archive"></i> <span>Inventory</span></a>
						<!-- fa fa fa-folder-o fa-fw -->
						<ul>
							<li>
								<a href="<?php echo url();?>/inventories/roindex">Request Order</a>
							</li>
							<li>
								<a href="<?php echo url();?>/inventories/approindex">Approval RO</a>
							</li>
							<li>
								<a href="<?php echo url();?>/inventories/poindex">Purchase Order</a>
							</li>
							<li>
								<a href="<?php echo url();?>/inventories/apppoindex">Approval PO</a>
							</li>
							<li>
								<a href="#">Received Order</a>
							</li>
							<li>
								<a href="#">Invoice</a>
							</li> 
						</ul>
					</li>
					<li>
						<a class="parent"><i class="fa fa-credit-card"></i> <span>Accounting</span></a>
						<!-- fa fa-tasks fa-fw fa-balance-scale fa-credit-card-->
						<ul>
							<li>
								<a href="#">Account Payable</a>
							</li>
							<li>
								<a href="#">Account Receivable</a>
							</li>
							<li>
								<a href="#">Bank Statment</a>
							</li>
							<li>
								<a href="#">General Ledger</a>
							</li>
							<li>
								<a href="#">Cash Journal</a>
							</li>
						</ul>
					</li>
					<li>
						<a class="parent"><i class="fa fa-line-chart fa-fw"></i> <span>Finance</span></a>
						<ul>
							<li>
								<a href="#">Pree Nursery</a>
							</li>
							<li>
								<a href="#">Activity Management</a>
							</li>
							<li>
								<a href="#">Machine Running Account</a>
							</li>
							<li>
								<a href="#">Field Budget</a>
							</li>
							<li>
								<a href="#">Palm Oil Production Budget</a>
							</li>
							<li>
								<a href="#">Vehicle Budget</a>
							</li>
						</ul>
					</li>
					<li>
						<a class="parent"><i class="fa fa-database"></i> <span>Master</span></a>
						<ul>
							<li>
								<a href="<?php echo url();?>/regions/index">Region</a>
							</li>
							<li>
								<a href="<?php echo url();?>/countries/index">Country</a>
							</li>
							<li>
								<a href="<?php echo url();?>/provinces/index">Province</a>
							</li>
							<li>
								<a href="<?php echo url();?>/cities/index">City</a>
							</li>
							<li>
								<a href="<?php echo url();?>/job/index">Job</a>
							</li>
							<li>
								<a href="<?php echo url();?>/employee/index">Employee</a>
							</li>
							<li>
								<a href="<?php echo url();?>/master/user">User App</a>
							</li>
							<li>
								<a href="<?php echo url();?>/master/supplier">Supplier</a>
							</li> 
							<li>
								<a href="<?php echo url();?>/master/goodscat">Goods Category</a>
							</li>
							<li>
								<a href="<?php echo url();?>/master/goods">Goods</a>
							</li>
							<li>
								<a href="<?php echo url();?>/master/customerscat">Customer Category</a>
							</li>
							<li>
								<a href="<?php echo url();?>/master/customers">Customer</a>
							</li>
							<!-- <li>
								<a href="<?php echo url();?>/master/coa">Chart Of Account</a>
							</li>  -->  
						</ul>
					</li>
				</ul>
			</nav>
			<div id="content">
				<div class="page-header">
					<div class="container-fluid">
						<h1>i-Groot</h1>
						<ul class="breadcrumb">
							<li>
								<small>E-Palm Management System Information</small> 
							</li>
							<!-- <li>
								<a href="#">&copy; <?php echo date('Y');?> </a>
							</li> -->
						</ul>
					</div> 
				</div>
				<?php if(isset($content)): echo $content; EndIf; ?>
			</div>
		</div>
		<footer id="footer">
			<a href="#">Groot Team</a> &copy; <?php echo date('Y');?> All Rights Reserved.
			<br />
			Version 1.01
		</footer>
		<div class="modal fade" id="loading">
            <div class='modal-dialog'>
               <div class='modal-body'>
                    <div class="sk-wave">
                      <div class="sk-rect sk-rect1"></div>
                      <div class="sk-rect sk-rect2"></div>
                      <div class="sk-rect sk-rect3"></div>
                      <div class="sk-rect sk-rect4"></div>
                      <div class="sk-rect sk-rect5"></div>
                    </div>
               </div>
            </div>
        </div>
	</body>
</html>