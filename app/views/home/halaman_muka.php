<div class="container-fluid">
	<?php if (Session::has('message')): ?>
	<div class="alert alert-success">
		<i class="fa fa-exclamation-circle"></i><small> <?php echo Session::get('message'); ?>.</small>
		<button type="button" class="close" data-dismiss="alert">
			×
		</button>
	</div>
	<?php EndIf; ?>
</div>