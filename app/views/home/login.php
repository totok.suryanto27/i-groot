<!DOCTYPE html>
<html dir="ltr" lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>i-Groot | Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
		<script type="text/javascript" src="assets/js/jquery/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap/js/bootstrap.min.js"></script>
		<link href="assets/js/bootstrap/less/bootstrap.less" rel="stylesheet/less" />
		<script src="assets/js/bootstrap/less-1.7.4.min.js"></script>
		<link href="assets/js/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
		<link href="assets/js/summernote/summernote.css" rel="stylesheet" />
		<script type="text/javascript" src="assets/js/summernote/summernote.js"></script>
		<script src="assets/js/jquery/datetimepicker/moment.js" type="text/javascript"></script>
		<script src="assets/js/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<link href="assets/js/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
		<link type="text/css" href="assets/css/stylesheet.css" rel="stylesheet" media="screen" /> 
		<script src="assets/js/common.js" type="text/javascript"></script>
		<link type="text/css" href="assets/css/login.css" rel="stylesheet" media="screen" />
		<link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/components.css" rel="stylesheet" type="text/css"/>
	</head> 

	<body class="login igroot-login"> 
		<div class="menu-toggler sidebar-toggler">
		</div> 
		<!-- BEGIN LOGO -->
		<div class="logo logo-img">
			<a href="index.html" class="">
			<img class="img-responsive" src="assets/img/i-groot_logo.png" alt=""/>
			</a>
		</div>
		<!-- END LOGO -->

		<!-- BEGIN LOGIN -->
		<div class="content box-login">
			<!-- BEGIN LOGIN FORM --> 
			<?php if (Session::has('message')): ?>
				<div class="alert alert-danger">
					<i class="fa fa-exclamation-circle"></i>
					<small> Wrong username or password , please try again !!</small>
					<button type="button" class="close" data-dismiss="alert">×</button>
				</div>
			<?php EndIf; ?>
			<form action="<?php echo url();?>/auth" method="post" class="login-form" enctype="multipart/form-data">
				<h3 class="form-title">Sign In</h3>
				<div class="alert alert-danger display-hide">
					<button class="close" data-close="alert"></button>
					<span>
					Enter any username and password. </span>
				</div>
				<div class="form-group">
					<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
					<label class="control-label visible-ie8 visible-ie9">Username</label>
					<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
				</div>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9">Password</label>
					<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-success uppercase red-btn">Login</button>
					<label class="rememberme check">
					<input type="checkbox" name="remember" value="1"/>Remember </label>
					
				</div>
				 
			</form>
			<!-- END LOGIN FORM --> 
		</div>
		
		<div class="copyright">
			 <?php echo date('Y');?> © Groot Team. i-Groot Apps.
		</div>
	</body>
</html>
