<div class="container-fluid">
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<div class="panel-body">
			<div class="row">
				<div class="pull-right">
					<a href="<?php echo url();?>/master/goods" data-toggle="tooltip" title="Back To Grid" class="btn btn-default"><i class="fa fa-refresh"></i> Back</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<?php if (Session::has('message')): ?>
			<div class="alert alert-danger">
				<i class="fa fa-exclamation-circle"></i><small>  <?php echo Session::get('message'); ?> !!</small>
				<button type="button" class="close" data-dismiss="alert">
					×
				</button>
			</div>
		<?php endif; ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Goods Category</h3>
			</div>
			<div class="panel-body">
			<form class="form-horizontal" method="POST" action="<?php echo url();?>/goods_save">
					 <input type="hidden" name="id" class="form-control" value="<?php echo $data->id;?>" />
					<div class="form-group required">
						<label class="col-sm-2 control-label">Goods Code</label>
						<div class="col-sm-6">
							<input type="text" name="kode_barang" class="form-control" value="<?php echo $data->kode_barang;?>" required/>
						</div>
					</div>
					<div class="form-group required">
						<label class="col-sm-2 control-label">Goods Name</label>
						<div class="col-sm-6">
							<input type="text" name="nama_barang" class="form-control" value="<?php echo $data->nama_barang;?>" required/>
						</div>
					</div> 
					<div class="form-group required">
						<label class="col-sm-2 control-label">Category Goods</label>
						<div class="col-sm-6">
							<select name="kategori_id" class="form-control" required>
								<option></option> 
								<?php foreach($type as $t): ?>
									<?php if($t->id==$data->type_id): ?>
										<option value="<?php echo $t->id;?>" selected><?php echo $t->nama_kategori;?></option>
									<?php Else: ?>
										<option value="<?php echo $t->id;?>"><?php echo $t->nama_kategori;?></option>
									<?php EndIf; ?>	
								<?php EndForeach; ?>	
							</select>
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-6"> 
							<textarea class="form-control" name="deskripsi" rows="5"><?php echo $data->deskripsi;?></textarea>
						</div>
					</div>
					<div class="form-group required">
						<label class="col-sm-2 control-label">Purchase Price</label>
						<div class="col-sm-6">
							<input type="text" name="harga_beli" class="form-control" value="<?php echo $data->harga_beli;?>" required/>
						</div>
					</div> 
					<div class="form-group required">
						<label class="col-sm-2 control-label">Selling Price</label>
						<div class="col-sm-6">
							<input type="text" name="harga_jual" value="<?php echo $data->harga_jual;?>" class="form-control" required/>
						</div>
					</div> 
					<div class="form-group required">
						<label class="col-sm-2 control-label">Unit</label>
						<div class="col-sm-6">
							<input type="text" name="satuan" value="<?php echo $data->satuan;?>" class="form-control" required/>
						</div>
					</div> 
					<div class="form-group required">
						<label class="col-sm-2 control-label">Stock</label>
						<div class="col-sm-6">
							<input type="text" name="jml_stok" value="<?php echo $data->jml_stok;?>" class="form-control" required/>
						</div>
					</div> 
					<div class="pull-right">
						<button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning">
							<i class="fa fa-refresh"></i> Reset
						</button>
						<button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
				</form> 
			</div>
		</div>
	</div>
</div>
