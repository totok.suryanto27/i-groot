<div class="container-fluid">
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<div class="panel-body">
			<div class="row">
				<div class="pull-right">
					<a href="<?php echo url();?>/master/goodsadd" data-toggle="tooltip" title="Add New" class="btn btn-default"><i class="fa fa-plus"></i> Add New</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Goods</h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-12">
							<form action="<?php echo url();?>/supplier/index">
								<div class="form-group">
									<label class="control-label" for="search">Goods Name</label>
									<input type="text" name="search" value="" placeholder="Goods Name" id="search" class="form-control" />
								</div>
								<button type="submit" id="button-filter" class="btn btn-primary pull-right">
									<i class="fa fa-search"></i> Filter
								</button>
							</form>
						</div>
					</div>
				</div>
				<?php 
					$no = 1; 
					if(isset($_GET['page'])){
						$no = ($_GET['page']*10)-9;
					}
				?>
				<?php if (Session::has('message')): ?>
				<div class="alert alert-success">
					<i class="fa fa-exclamation-circle"></i><small>  <?php echo Session::get('message'); ?> !!</small>
					<button type="button" class="close" data-dismiss="alert">
						×
					</button>
				</div>
				<?php endif; ?>
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Goods Code</th>
								<th>Goods Name</th> 
								<!-- <th>Category Goods</th> -->
								<th>Description</th> 
								<!-- <th>Input Date</th>   -->
								<th>Purchase Price</th>
								<th>Selling Price</th> 
								<th>Unit</th>
								<th>Stock</th> 
								<th>Actions</th>
							</tr>
						</thead>
						<tbody> 
							<?php foreach($data as $row): ?>
							<tr>
								<td><?php echo $no;?></td>
								<td><?php echo $row->kode_barang;?></td> 
								<td><?php echo $row->nama_barang;?></td>   
								<td><?php echo $row->deskripsi;?></td>  
								<!-- <td><?php echo $row->tgl_input;?></td>  -->
								<td><?php echo $row->harga_beli;?></td>  
								<td><?php echo $row->harga_jual;?></td> 
								<td><?php echo $row->satuan;?></td>  
								<td><?php echo $row->jml_stok;?></td>  
								<td>
									<a href="<?php echo url();?>/master/goodsedit/<?php echo $row->id;?>"><i class="fa fa-pencil"></i></a> 
									<a href="<?php echo url();?>/master/goodsdelete/<?php echo $row->id;?>" onclick="return confirm('Are you sure ??');"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<?php $no++; EndForeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="pagination">
			<?php 
				if(isset($_GET['search'])){
					echo $data->appends(array('search' => $_GET['search']))->links();	
				}else{
					echo $data->links();
				}	
			?>
		</div>
	</div>
</div>