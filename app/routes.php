<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');
Route::get('/logout', 'HomeController@getLogout');
Route::get('/dashboard', 'HomeController@getDashboard');
Route::post('auth', 'HomeController@getAuth');
Route::controller('users', 'UsersController');
Route::post('users', 'UsersController@getSave');
Route::controller('master', 'MasterController');

// Supplier
Route::get('supplier', 'MasterController@getSupplier');
Route::post('supplier_save', 'MasterController@getSuppliersave');

// Kategori Barang
Route::get('goodscat', 'MasterController@getGoodscat');
Route::post('goodscat_save', 'MasterController@getGoodscatsave');

// Barang
Route::get('goods', 'MasterController@getGoods');
Route::post('goods_save', 'MasterController@getGoodssave');

// Kategori Pelanggan
Route::get('customerscat', 'MasterController@getCustomerscat');
Route::post('customerscat_save', 'MasterController@getCustomerscatsave');

// Pelanggan
Route::get('customers', 'MasterController@getCustomers');
Route::post('customers_save', 'MasterController@getCustomerssave');

Route::controller('employee', 'EmployeeController');
Route::post('employee', 'EmployeeController@getSave');

Route::controller('regions', 'RegionsController');
Route::post('regions', 'RegionsController@getSave');

Route::controller('cities', 'CitiesController');
Route::post('cities', 'CitiesController@getSave');

Route::controller('provinces', 'ProvincesController');
Route::post('provinces', 'ProvincesController@getSave');

Route::controller('countries', 'CountriesController');
Route::post('countries', 'CountriesController@getSave');

Route::controller('job', 'JobController');
Route::post('job', 'JobController@getSave');

Route::controller('inventories', 'InventoriesController');
Route::post('inventories', 'InventoriesController@getSavereq');
Route::post('apvro', 'InventoriesController@getSaveappreq');
Route::post('po', 'InventoriesController@getSavepo');
Route::post('apvpo', 'InventoriesController@getSaveapppurc');

// Route::controller('guest', 'GuestController');
// Route::post('guest', 'GuestController@getSave');

// Route::controller('hotel', 'HotelController');
// Route::post('hotel', 'HotelController@getSave');

// Route::controller('identity', 'IdentityController');
// Route::post('identity', 'IdentityController@getSave');

// Route::controller('type', 'TypeController');
// Route::post('type', 'TypeController@getSave');

// Route::controller('rooms', 'RoomsController');
// Route::post('rooms', 'RoomsController@getSave');

// Route::controller('services', 'ServicesController');
// Route::post('services', 'ServicesController@getSave');
 
// Route::controller('payments', 'PaymentsController');
