<?php

class Goods extends Eloquent{

	protected $table = 'goods';

	public function goodcat(){
        return $this->belongsTo('Goodscats'); 
    } 

    public function requsestproduct(){
        return $this->hasMany('Requestproduct');
    }
}