<?php

class Requestproduct extends Eloquent{

	protected $table = 'request_product';

	public function goods(){
        return $this->belongsTo('Goods');
    }
 
}