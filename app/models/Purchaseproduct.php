<?php

class Purchaseproduct extends Eloquent{

	protected $table = 'purchase_product';

	public function goods(){
        return $this->belongsTo('Goods');
    }

	public function supplier(){
        return $this->belongsTo('supplier');
    }
 
}