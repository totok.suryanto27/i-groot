<?php

class Inventories extends Eloquent{

	protected $table = 'request';

	public function status(){
        return $this->belongsTo('State');
    }

    public function type(){
        return $this->belongsTo('Type');
    }
 	
 	public function reqproduct(){
        return $this->hasMany('Requestproduct');
    }

    public function reqresponsible(){
        return $this->hasMany('Requestresponsible');
    }

}