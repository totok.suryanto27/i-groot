<?php

class Purchases extends Eloquent{

	protected $table = 'purchase';

	public function status(){
        return $this->belongsTo('State');
    }

    public function type(){
        return $this->belongsTo('Type');
    }
 	
 	// public function reqproduct(){
  //       return $this->hasMany('Purchaseproduct');
  //   }

  //   public function reqresponsible(){
  //       return $this->hasMany('Purchaseresponsible');
    // }

}